function open_video(){
  $.magnificPopup.open({
    items: {
      src: 'https://vimeo.com/471716560'
    },
    type: 'iframe'
  });
}

function galeria_home_page(){
  $(".home-galeria-thumb").fancybox({
		openEffect	: 'elastic',
    closeEffect	: 'elastic',
    prevEffect	: 'fade',
		nextEffect	: 'fade',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
  });
}

function fetch_data_home_page(field_select, section){
  var select = field_select;
  $.ajax({
    url:"/home_page/fetch_data?page="+paginacao_home+"&select="+select,
    beforeSend: function(){
        document.getElementById(section).innerHTML = '<div class="card-body w-100">'
            + '<div class="table-responsive text-center">'
            + '<img src="../assets/icons/loading.gif">'
            + '</div>'
            + '</div>';
    },
    success:function(data){
        document.getElementById(section).innerHTML = data;
    }
  });
}
