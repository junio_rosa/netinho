//=================================================ATUALIZAR PERFIL======================================
function modal_atualizar_perfil(obj){
    document.getElementById('updateUser_id').value = obj.id;
    document.getElementById('updateUser_nome').value = obj.name;
    document.getElementById('updateUser_email').value = obj.email;
    document.getElementById('updateUser_senha').value = '';
    document.getElementById('updateUser_repita_senha').value = '';
    document.getElementById('updateUser_id').classList.add('d-none');
    validar_campos_form_atualizar_perfil();
    $('#modal_update_user').modal({backdrop: 'static', keyboard: false});
}

function validar_campos_form_atualizar_perfil(){
    validacao_campo('updateUser_nome', 'updateUser_small_nome', 'Campo Validado', 'Campo nome inválido', 1);
    validacao_email('updateUser_email', 'updateUser_small_email', 'Campo Validado', 'Campo e-mail inválido');
    validacao_senhas('updateUser_senha', 'updateUser_repita_senha', 'updateUser_small_senha', 'updateUser_small_repita_senha');
}

function atualizar_perfil_valido(){
    controle = true;
    validar_campos_form_atualizar_perfil();
    nome = document.getElementById('updateUser_small_nome').innerHTML;
    email = document.getElementById('updateUser_small_email').innerHTML;
    senha = document.getElementById('updateUser_small_senha').innerHTML;
    repita_senha = document.getElementById('updateUser_small_repita_senha').innerHTML;
    msg_erro = '';
    if(nome != 'Campo Validado'){
        msg_erro += nome+'<br>';
    }
    if(email != 'Campo Validado'){
        msg_erro += email+'<br>';
    }
    if(senha != 'Campo Validado'){
        msg_erro += senha+'<br>';
    }
    if(repita_senha != 'As senhas são iguais'){
        msg_erro += repita_senha+'<br>';
    }
    if(msg_erro != ''){
        document.getElementById('snackbar1').innerHTML = 'ERROS:<br><br>'+msg_erro;
        exibir_snackbar('snackbar1');
        controle = false;
    }
    return controle;
}

function action_update_user() {
    if(atualizar_perfil_valido()){
        user = {
            id: document.getElementById('updateUser_id').value,
            nome: document.getElementById('updateUser_nome').value,
            email: document.getElementById('updateUser_email').value,
            senha: document.getElementById('updateUser_senha').value,
        }
        $.ajax({
            method: 'POST',
            url: 'atualizar_perfil',
            data: user,
            success: function(msg){
                document.getElementById('snackbar1').innerHTML = msg;
                exibir_snackbar('snackbar1');
                if(msg == 'Perfil atualizado com sucesso'){
                    atualizar_navbar();
                }
            }
        });
    }
}

function atualizar_navbar(){
    $.ajax({
        method: 'GET',
        url: 'atualizar_navbar/',
        success:function(data){
            document.getElementById('header_navbar').innerHTML = data;
        }
    });
}
//=================================================ATUALIZAR PERFIL======================================

//=================================================ESQUECEU SENHA======================================
$("#form_esqueceu_senha").on('submit', function (e) {
    e.preventDefault();
    validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido');
    email = document.getElementById('small_email').innerHTML;
    if(email != 'Campo e-mail inválido'){
        document.getElementById('form_esqueceu_senha').submit();
    }else{
        document.getElementById('snackbar2').innerHTML = 'ERROS:<br><br>'+email;
        exibir_snackbar('snackbar2');
    }
});
//=================================================ESQUECEU SENHA======================================

//=================================================RESETAR SENHA======================================
$("#form_reset_senha").on('submit', function (e) {
    e.preventDefault();
    if(reset_senha_valido()){
        document.getElementById('form_reset_senha').submit();
    }
});

function reset_senha_valido(){
    validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido');
    validacao_campo('senha', 'small_senha', 'Campo Validado', 'Campo senha inválido', 8);
    validacao_senhas('senha', 'repita_senha', 'small_senha', 'small_repita_senha');
    var email = document.getElementById('small_email').innerHTML;
    var senha = document.getElementById('small_senha').innerHTML;
    var repita_senha = document.getElementById('small_repita_senha').innerHTML;
    var controle = true;
    var msg_erro = '';
    if(email == 'Campo e-mail inválido'){
        msg_erro += email+'<br>';
    }
    if(senha != 'Campo Validado'){
        msg_erro += senha+'<br>';
    }
    if(repita_senha != 'As senhas são iguais'){
        msg_erro += repita_senha+'<br>';
    }
    if(msg_erro != ''){
        document.getElementById('snackbar2').innerHTML = 'ERROS:<br><br>'+msg_erro;
        exibir_snackbar('snackbar2');
        controle = false;
    }
    return controle;
}
//=================================================RESETAR SENHA======================================
