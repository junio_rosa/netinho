function open_form_cas(obj, galeria, datas_marcadas){
    galeriaFileArray.length = 0;
    img_viewer_control = 0;
    datas_bd.length = 0;
    datas_para_deletar.length = 0;
    datas_para_upload.length = 0;
    if(obj == 0){
        var array_input = ['form_cas_nome', 'form_cas_endereco', 'form_cas_ocupantes'];
        var array_small = [
            {
                small: 'small_cas_nome',
                msg: 'Digite o nome da casa'
            },

            {
                small: 'small_cas_endereco',
                msg: 'Digite o endereço da casa'
            },

            {
                small: 'small_cas_ocupantes',
                msg: 'Digite o número máximo de ocupantes'
            }
        ];
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        $('#form_cas_descricao').jqteVal("");
        limpar_campos_formulario(array_input, array_small);
        document.getElementById('imgDestaque').src = "assets/icons/sem_img.png";
        document.getElementById('imgDestaqueLink').href = "assets/icons/sem_img.png";
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_cas_churrasqueira').checked = false;
        document.getElementById('form_cas_piscina').checked = false;
        document.getElementById('form_cas_sauna').checked = false;
        document.getElementById('form_cas_title').innerHTML = 'Adicionar Casa';
        document.getElementById('form_cas_button').innerHTML = '<i class="fas fa-plus-circle text-white"></i> Cadastrar';
    }else{
        var i = 0;
        while(i < datas_marcadas.length){
            datas_bd.push(datas_marcadas[i].data_aluguel);
            i++;
        }
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        document.getElementById('form_cas_title').innerHTML = 'Editar Casa';
        document.getElementById('form_cas_button').innerHTML = '<i class="fas fa-sync"></i> Atualizar';
        document.getElementById('imgDestaque').src = obj.imagem_destaque;
        document.getElementById('imgDestaqueLink').href = obj.imagem_destaque;
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_cas_id').value = obj.id;
        document.getElementById('form_cas_nome').value = obj.nome;
        document.getElementById('form_cas_endereco').value = obj.endereco;
        document.getElementById('form_cas_ocupantes').value = obj.max_ocupantes;
        $('#form_cas_descricao').jqteVal(obj.descricao);
        document.getElementById('form_cas_churrasqueira').checked = obj.churrasqueira;
        document.getElementById('form_cas_piscina').checked = obj.piscina;
        document.getElementById('form_cas_sauna').checked = obj.sauna;
        validacao_form_cas();
        i = 0;
        while(i < galeria.length){
            galeriaImgPreview(galeria[i].url_imagem, galeria[i], 0, 0);
            i++;
        }
    }
    var data_atual = new Date();
    montar_calendario(data_atual, 0);
    $('#form_cas_modal').modal({backdrop: 'static', keyboard: false});
}

function validacao_form_cas(){
    validacao_campo('form_cas_nome', 'small_cas_nome', 'Campo Validado', 'Nome inválido', 1);
    validacao_campo('form_cas_endereco', 'small_cas_endereco', 'Campo Validado', 'Endereço inválido', 1);
    validacao_campo('form_cas_ocupantes', 'small_cas_ocupantes', 'Campo Validado', 'Campo ocupantes inválido', 1);
}

function form_cas_valido(){
    var control = true;
    validacao_form_cas();
    var nome = document.getElementById('small_cas_nome').innerHTML;
    var endereco = document.getElementById('small_cas_endereco').innerHTML;
    var ocupantes = document.getElementById('small_cas_ocupantes').innerHTML;
    var imagem_destaque = document.getElementById('input_imgDestaque').files;
    var msg_erro = '';
    if(nome != 'Campo Validado'){
        msg_erro += nome+'<br>';
    }
    if(endereco != 'Campo Validado'){
        msg_erro += endereco+'<br>';
    }
    if(ocupantes != 'Campo Validado'){
        msg_erro += ocupantes+'<br>';
    }
    if(imagem_destaque.length != 0){
        if(!imagem_destaque[0].type.includes('image')){
            msg_erro += 'Formato de imagem inválido para upload<br>';
        }
    }
    if(msg_erro != ''){
        document.getElementById('snackbar2').innerHTML = 'ERROS:<br><br>'+msg_erro;
        exibir_snackbar('snackbar2');
        control = false;
    }
    return control;
}

function action_form_cas(){
    var button = document.getElementById('form_cas_button').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-white"></i> Cadastrar'){
        cadastrar_casa();
    }else{
        atualizar_casa();
    }
}

function cadastrar_casa(){
    if(form_cas_valido()){
        casa = new FormData();
        casa.append('nome', document.getElementById('form_cas_nome').value);
        casa.append('endereco', document.getElementById('form_cas_endereco').value);
        casa.append('max_ocupantes', document.getElementById('form_cas_ocupantes').value);
        casa.append('descricao', $('#form_cas_descricao').val());
        casa.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('form_cas_churrasqueira').checked){
            casa.append('churrasqueira', 1);
        }else{
            casa.append('churrasqueira', 0);
        }
        if(document.getElementById('form_cas_piscina').checked){
            casa.append('piscina', 1);
        }else{
            casa.append('piscina', 0);
        }
        if(document.getElementById('form_cas_sauna').checked){
            casa.append('sauna', 1);
        }else{
            casa.append('sauna', 0);
        }
        if(document.getElementById('input_imgDestaque').files.length != 0){
            casa.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            casa.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var pos_array;
        var table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                casa.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                casa.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }
            i++;
        }
        casa.append('tamanho_galeria', tam_uploaded);
        casa.append('datas_para_upload', datas_para_upload);
        $.ajax({
            type:"POST",
            url:'casas',
            data:casa,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success:function(msg){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = msg;
                exibir_snackbar('snackbar2');
                if(msg == 'Casa cadastrada com sucesso'){
                    fetch_data_casas();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function atualizar_casa(){
    if(form_cas_valido()){
        casa = new FormData();
        casa.append('id', document.getElementById('form_cas_id').value);
        casa.append('nome', document.getElementById('form_cas_nome').value);
        casa.append('endereco', document.getElementById('form_cas_endereco').value);
        casa.append('max_ocupantes', document.getElementById('form_cas_ocupantes').value);
        casa.append('descricao', $('#form_cas_descricao').val());
        casa.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('form_cas_churrasqueira').checked){
            casa.append('churrasqueira', 1);
        }else{
            casa.append('churrasqueira', 0);
        }
        if(document.getElementById('form_cas_piscina').checked){
            casa.append('piscina', 1);
        }else{
            casa.append('piscina', 0);
        }
        if(document.getElementById('form_cas_sauna').checked){
            casa.append('sauna', 1);
        }else{
            casa.append('sauna', 0);
        }
        if(document.getElementById('input_imgDestaque').files.length != 0){
            casa.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            casa.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var tam_deleted = 0;
        var table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0' && table[i].cells[3].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                casa.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                casa.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }else{
                if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '1' && table[i].cells[3].textContent == '1'){
                    casa.append('deletar_galeria_foto_'+tam_deleted, table[i].cells[4].textContent);
                    tam_deleted++;
                    table[i].cells[1].textContent = "";
                    table[i].cells[2].textContent = "";
                    table[i].cells[3].textContent = "";
                    table[i].cells[4].textContent = "";
                    table[i].cells[5].textContent = "";
                    table[i].cells[6].textContent = "";
                }
            }
            i++;
        }
        casa.append('tamanho_galeria', tam_uploaded);
        casa.append('tamanho_deletar_galeria', tam_deleted);
        casa.append('datas_para_upload', datas_para_upload);
        casa.append('datas_para_deletar', datas_para_deletar);
        $.ajax({
            type: 'POST',
            url: 'casas/update',
            data: casa,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success: function(array){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = array.msg;
                exibir_snackbar('snackbar2');
                if(array.msg == 'Casa atualizada com sucesso'){
                    i = 0;
                    datas_bd.length = 0;
                    while(i < array.datas_bd.length){
                        datas_bd.push(array.datas_bd[i].data_aluguel);
                        i++;
                    }
                    datas_para_deletar.length = 0;
                    datas_para_upload.length = 0;
                    var data_atual = new Date();
                    montar_calendario(data_atual, 0);

                    table = $('#table_galeria>tbody>tr');
                    i = 0;
                    while(i < table.length){
                        if(table[i].cells[3].textContent == '0'){
                            document.getElementById('galeria_div_'+i).remove();
                            table[i].remove();
                        }
                        i++;
                    }
                    galeriaFileArray.length = 0;
                    i = 0;
                    while(i < array.novas_fotos.length){
                        galeriaImgPreview(array.novas_fotos[i].url_imagem, array.novas_fotos[i], 0, 0);
                        i++;
                    }
                    ocultar_galeria();
                    fetch_data_casas();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function fetch_data_casas(){
    $('#cas_autocomplete').fadeOut();
    page = document.getElementById('paginacao_casas').value;
    input = document.getElementById('cas_search').value;
    select = document.getElementById('cas_select').value;
    $.ajax({
        url:"/casas/fetch_data?page="+page+"&input="+input+"&select="+select,
        beforeSend: function(){
            document.getElementById('tabela_casas').innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/icons/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById('tabela_casas').innerHTML = data;
        }
    });
}

$('#cas_search').keyup(function(){
    input = $(this).val();
    select = document.getElementById('cas_select').value;
    if (event.keyCode === 13) {
        fetch_data_casas();
    }else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('casas/autocomplete', search, function(casas){
                document.getElementById('cas_autocomplete').innerHTML ='';
                if(casas.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#cas_autocomplete').append(data);
                    casas.forEach(function(casas){
                        data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\">"
                                    +"<img class= 'rounded img-autocomplete' src='"+casas.imagem_destaque+"'> "
                                    +"<strong>Nome:</strong> "
                                    +casas.nome+" <strong>Endereço: </strong>"
                                    +casas.endereco+" <strong></button></li>");
                        data.find('button').click(function() {
                            preenche_cas_input(casas);
                        });
                        $('#cas_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#cas_autocomplete').append(data);
                    $('#cas_autocomplete').fadeIn();
                }
            });
        }
    }
});

function preenche_cas_input(casa){
    select = document.getElementById('cas_select').value;
    field = casa.nome;
    if(select.includes('endereco')){
        field = casa.endereco;
    }
    document.getElementById('cas_search').value = field;
    $('#cas_autocomplete').fadeOut();
}

function open_confirm_cas(casa, option){
    document.getElementById('confirm_cas_id').value = casa.id;
    if(option){
        document.getElementById('confirm_cas_msg').innerHTML = "Você tem certeza que deseja desabilitar a casa "+casa.nome+" ?";
        document.getElementById('confirm_cas_title').innerHTML = 'Desabilitar Casa';
        document.getElementById('confirm_cas_button').innerHTML = '<i class="fas fa-minus-circle text-white"></i> Desabilitar';
    }else{
        document.getElementById('confirm_cas_msg').innerHTML = "Você tem certeza que deseja habilitar a casa "+casa.nome+" ?";
        document.getElementById('confirm_cas_title').innerHTML = 'Habilitar Casa';
        document.getElementById('confirm_cas_button').innerHTML = '<i class="fas fa-check-circle"></i> Habilitar';
    }
    $('#confirm_cas_modal').modal({backdrop: 'static', keyboard: false});
}

function action_confirm_cas(){
    var button = document.getElementById('confirm_cas_button').innerHTML;
    if(button == '<i class="fas fa-minus-circle text-white"></i> Desabilitar'){
        desabilitar_casa();
    }else{
        habilitar_casa();
    }
}

function desabilitar_casa(){
    id = document.getElementById('confirm_cas_id').value;
    $.ajax({
        type: 'DELETE',
        url: 'casas/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Casa desabilitada com sucesso'){
                fetch_data_casas();
            }
        }
    });
}

function habilitar_casa(){
    id = document.getElementById('confirm_cas_id').value;
    $.ajax({
        type: 'GET',
        url: 'casas/create/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Casa habilitada com sucesso'){
                fetch_data_casas();
            }
        }
    });
}

function open_modal_casa_details(obj, galeria, calendario){
    document.getElementById('details_cas_img').src = '../'+obj.imagem_destaque;
    document.getElementById('details_cas_img_link').href = '../'+obj.imagem_destaque;
    document.getElementById('details_cas_nome').innerHTML = obj.nome;
    document.getElementById('details_cas_endereco').innerHTML = '<i class="fas fa-map-marker-alt"></i> <strong>Endereço: </strong>'+ obj.endereco;
    document.getElementById('details_cas_max_ocupantes').innerHTML = '<i class="fas fa-male"></i> <strong>Lotação: </strong>'+ obj.max_ocupantes;
    document.getElementById('details_cas_piscina').innerHTML = (obj.piscina ? '<i class="far fa-check-square"></i>' : '<i class="far fa-square"></i>') + "<strong> Possui piscina</strong>"; 
    document.getElementById('details_cas_sauna').innerHTML = (obj.sauna ? '<i class="far fa-check-square"></i>' : '<i class="far fa-square"></i>') + "<strong> Possui sauna</strong>";
    document.getElementById('details_cas_churrasqueira').innerHTML = (obj.churrasqueira ? '<i class="far fa-check-square"></i>' : '<i class="far fa-square"></i>') + "<strong> Possui churrasqueira</strong>";
    document.getElementById('details_cas_descricao').innerHTML = obj.descricao;
    $('#modal_casa_details').modal();
  
    var i = 0;
    img_viewer_control = 0;
    document.getElementById('galeria_divContent').innerHTML = "";
    while(i < galeria.length){
        galeriaImgPreview('../'+galeria[i].url_imagem, galeria[i], 0, 1);
        i++;
    }
    i = 0;
    while(i < calendario.length){
        datas_bd.push(calendario[i].data_aluguel);
        i++;
    }
    montar_calendario(new Date(), 1);
}
