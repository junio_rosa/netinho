function montar_calendario(data_atual, option){
    $('#calendario>thead').remove();
    $('#calendario>tbody>tr').remove();
    cabecalho_calendario(data_atual, option);
    corpo_calendario(data_atual, option);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function cabecalho_calendario(data_atual, option){
    var array_mes = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    var template =  $('<thead>'+
                        '<tr class="text-center">'+
                            '<th colspan="5" class="text-center">'+
                                '<h5>'+
                                    '<i class="fas fa-chevron-left cursor-pointer" id="prev_mes"></i>'+
                                        '<strong class="font-weight-bold" id="mes">'+
                                            array_mes[data_atual.getMonth()]+
                                        '</strong>'+
                                    '<i class="fas fa-chevron-right cursor-pointer" id="next_mes"></i>'+
                                '</h5>'+
                            '</th>'+
                            '<th colspan="2" class="text-center">'+
                                '<h5>'+
                                    '<i class="fas fa-chevron-left cursor-pointer" id="prev_ano"></i>'+
                                        '<strong class="font-weight-bold" id="ano">'+
                                            data_atual.getFullYear()+
                                        '</strong>'+
                                    '<i class="fas fa-chevron-right cursor-pointer" id="next_ano"></i>'+
                                '</h5>'+
                            '</th>'+
                        '</tr>'+
                        '<tr class="text-center">'+
                            '<th>Dom</th>'+
                            '<th>Seg</th>'+
                            '<th>Ter</th>'+
                            '<th>Qua</th>'+
                            '<th>Qui</th>'+
                            '<th>Sex</th>'+
                            '<th>Sab</th>'+
                        '</tr>'+
                    '</thead>');
    template.find('#prev_mes').click(function() {
        var data_mes_anterior = new Date(data_atual.setMonth(data_atual.getMonth()-1));
        montar_calendario(data_mes_anterior, option);
    });
    template.find('#next_mes').click(function() {
        var data_proximo_mes = new Date(data_atual.setMonth(data_atual.getMonth()+1));
        montar_calendario(data_proximo_mes, option);
    });
    template.find('#prev_ano').click(function() {
        var data_ano_anterior = new Date(data_atual.setFullYear(data_atual.getFullYear()-1));
        montar_calendario(data_ano_anterior, option);
    });
    template.find('#next_ano').click(function() {
        var data_proximo_ano = new Date(data_atual.setFullYear(data_atual.getFullYear()+1));
        montar_calendario(data_proximo_ano, option);
    });
    $('#calendario').append(template);
}

function corpo_calendario(data_atual, option){
    var ultimo_dia = ultimo_dia_mes(data_atual);
    var dia_semana_inicio =  dia_semana_inicio_mes(data_atual);
    var i = 0;
    var dias = 1;
    var mes_id = data_atual.getMonth()+1;
    var id_data;
    var bg;
    var dia_atual = new Date();
    dia_atual = formatDate(dia_atual);
    if(mes_id < 10){
        mes_id = '0'+mes_id;
    }
    var template = '<tr>';
    while(i < 7){
        if(i < dia_semana_inicio){
            template += '<td></td>';
        }else{
            var dia_id = '0'+dias;
            id_data = data_atual.getFullYear()+"-"+mes_id+"-"+dia_id;
            if(datas_bd.includes(id_data) || datas_para_upload.includes(id_data)){
                bg = 'bg-danger text-white';
                if(datas_para_deletar.includes(id_data)){
                    bg = 'bg-white';
                }
            }else{
                bg = 'bg-white';
                if(id_data == dia_atual){
                    bg = 'bg-success text-white';
                }
            }
            if(option){
                template += "<td class='text-center "+bg+"' id='"+id_data+"'>"+
                                dia_id+
                            "</td>";
            }else{
                template += "<td class='text-center cursor-pointer "+bg+"' id='"+id_data+"' onclick=mark_date('"+id_data+"')>"+
                                dia_id+
                            "</td>";
            }
            dias++;
        }
        i++;
    }
    template += '</tr>';
    while(dias < ultimo_dia){
        i = 0;
        template += '<tr>';
        while(i < 7){
            dia_id = dias;
            if(dias < 10){
                dia_id = '0'+dias;
            }
            if(dias <= ultimo_dia){
                id_data = data_atual.getFullYear()+"-"+mes_id+"-"+dia_id;
                if(datas_bd.includes(id_data) || datas_para_upload.includes(id_data)){
                    bg = 'bg-danger text-white';
                    if(datas_para_deletar.includes(id_data)){
                        bg = 'bg-white';
                    }
                }else{
                    bg = 'bg-white';
                    if(id_data == dia_atual){
                        bg = 'bg-success text-white';
                    }
                }
                if(!option){
                    template += "<td class='text-center cursor-pointer "+bg+"' id='"+id_data+"' onclick=mark_date('"+id_data+"')>"+
                                    dia_id+
                                "</td>";
                }else{
                    template += "<td class='text-center cursor-pointer "+bg+"' id='"+id_data+"'>"+
                                    dia_id+
                                "</td>";
                }
            }else{
                template += "<td></td>";
            }
            i++;
            dias++;
        }
        template += '</tr>';
    }
    $('#calendario>tbody').append(template);
}

function ultimo_dia_mes(data_atual){
    /* 0 é Domingo, 6 é Sábado */
    var ultimo_dia_mes = new Date(data_atual.getFullYear(), data_atual.getMonth() + 1, 0);
    ultimo_dia_mes = String(ultimo_dia_mes.getDate()).padStart(2, '0');
    return ultimo_dia_mes;
}

function dia_semana_inicio_mes(data_atual){
    var primeiro_dia_semana_do_mes = new Date(data_atual.getFullYear(), data_atual.getMonth(), 1);
    return primeiro_dia_semana_do_mes = primeiro_dia_semana_do_mes.getDay();
}

function mark_date(data){
    var conferir_array;
    if(document.getElementById(data).classList.contains('bg-white') ||
        document.getElementById(data).classList.contains('bg-success')){
        conferir_array = datas_bd.indexOf(data);
        if (conferir_array >= 0) {
            conferir_array = datas_para_deletar.indexOf(data);
            datas_para_deletar.splice(conferir_array, 1);
        }else{
            datas_para_upload.push(data);
        }
        document.getElementById(data).classList.remove('bg-success');
        document.getElementById(data).classList.remove('text-white');
        document.getElementById(data).classList.remove('bg-white');
        document.getElementById(data).classList.add('bg-danger');
        document.getElementById(data).classList.add('text-white');
    }else{
        conferir_array = datas_bd.indexOf(data);
        if (conferir_array >= 0) {
            datas_para_deletar.push(data);
        }else{
            conferir_array = datas_para_upload.indexOf(data);
            datas_para_upload.splice(conferir_array, 1);
        }
        var dia_atual = new Date();
        dia_atual = formatDate(dia_atual);
        if(data != dia_atual){
            document.getElementById(data).classList.remove('bg-danger');
            document.getElementById(data).classList.remove('text-white');
            document.getElementById(data).classList.add('bg-white');
        }else{
            document.getElementById(data).classList.remove('bg-danger');
            document.getElementById(data).classList.add('bg-success');
        }
    }
}
