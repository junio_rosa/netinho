function open_form_pou(obj, galeria){
    galeriaFileArray.length = 0;
    img_viewer_control = 0;
    if(obj == 0){
        var array_input = ['form_pou_nome', 'form_pou_endereco', 'form_pou_diaria', 'form_pou_telefone', 'form_pou_tamanho'];
        var array_small = [
            {
                small: 'small_pou_nome',
                msg: 'Digite o nome da pousada'
            },

            {
                small: 'small_pou_endereco',
                msg: 'Digite o endereço da pousada'
            },

            {
                small: 'small_pou_diaria',
                msg: 'Digite o valor da diária'
            },

            {
                small: 'small_pou_telefone',
                msg: 'Digite o telefone para contato'
            },

            {
                small: 'small_pou_tamanho',
                msg: 'Digite o tamanho e a quantidade de quartos'
            }
        ];
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        document.getElementById('form_pou_tamanho').value = '';
        document.getElementById('form_pou_piscina').checked = false;
        $('#form_pou_descricao').jqteVal("");
        limpar_campos_formulario(array_input, array_small);
        document.getElementById('imgDestaque').src = "assets/icons/sem_img.png";
        document.getElementById('imgDestaqueLink').href = "assets/icons/sem_img.png";
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_pou_title').innerHTML = 'Adicionar Pousada';
        document.getElementById('form_pou_button').innerHTML = '<i class="fas fa-plus-circle text-white"></i> Cadastrar';
    }else{
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        document.getElementById('form_pou_title').innerHTML = 'Editar Pousada';
        document.getElementById('form_pou_button').innerHTML = '<i class="fas fa-sync"></i> Atualizar';
        document.getElementById('imgDestaque').src = obj.imagem_destaque;
        document.getElementById('imgDestaqueLink').href = obj.imagem_destaque;
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_pou_id').value = obj.id;
        document.getElementById('form_pou_piscina').checked = obj.piscina;
        document.getElementById('form_pou_nome').value = obj.nome;
        document.getElementById('form_pou_endereco').value = obj.endereco;
        document.getElementById('form_pou_diaria').value = obj.diaria;
        document.getElementById('form_pou_telefone').value = obj.telefone;
        $('#form_pou_descricao').jqteVal(obj.descricao);
        document.getElementById('form_pou_tamanho').value = obj.tamanho;
        validacao_form_pou();
        var i = 0;
        while(i < galeria.length){
            galeriaImgPreview(galeria[i].url_imagem, galeria[i], 0, 0);
            i++;
        }
    }
    $('#form_pou_modal').modal({backdrop: 'static', keyboard: false});
}

function validacao_form_pou(){
    validacao_campo('form_pou_tamanho', 'small_pou_tamanho', 'Campo Validado', 'Campo tamanho dos quartos inválido', 1);
    validacao_campo('form_pou_nome', 'small_pou_nome', 'Campo Validado', 'Nome inválido', 1);
    validacao_campo('form_pou_endereco', 'small_pou_endereco', 'Campo Validado', 'Endereço inválido', 1);
    validacao_campo('form_pou_diaria', 'small_pou_diaria', 'Campo Validado', 'Valor da diária inválido', 1);
    validacao_campo('form_pou_telefone', 'small_pou_telefone', 'Campo Validado', 'Telefone inválido', 14)
}

function form_pou_valido(){
    var control = true;
    validacao_form_pou();
    var nome = document.getElementById('small_pou_nome').innerHTML;
    var endereco = document.getElementById('small_pou_endereco').innerHTML;
    var diaria = document.getElementById('small_pou_diaria').innerHTML;
    var telefone = document.getElementById('small_pou_telefone').innerHTML;
    var tamanho = document.getElementById('small_pou_tamanho').innerHTML;
    var imagem_destaque = document.getElementById('input_imgDestaque').files;
    var msg_erro = '';
    if(nome != 'Campo Validado'){
        msg_erro += nome+'<br>';
    }
    if(endereco != 'Campo Validado'){
        msg_erro += endereco+'<br>';
    }
    if(diaria != 'Campo Validado'){
        msg_erro += diaria+'<br>';
    }
    if(telefone != 'Campo Validado'){
        msg_erro += telefone+'<br>';
    }
    if(tamanho != 'Campo Validado'){
        msg_erro += tamanho+'<br>';
    }
    if(imagem_destaque.length != 0){
        if(!imagem_destaque[0].type.includes('image')){
            msg_erro += 'Formato de imagem inválido para upload<br>';
        }
    }
    if(msg_erro != ''){
        document.getElementById('snackbar2').innerHTML = 'ERROS:<br><br>'+msg_erro;
        exibir_snackbar('snackbar2');
        control = false;
    }
    return control;
}

function action_form_pou(){
    var button = document.getElementById('form_pou_button').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-white"></i> Cadastrar'){
        cadastrar_pousada();
    }else{
        atualizar_pousada();
    }
}

function cadastrar_pousada(){
    if(form_pou_valido()){
        pousada = new FormData();
        pousada.append('nome', document.getElementById('form_pou_nome').value);
        pousada.append('endereco', document.getElementById('form_pou_endereco').value);
        pousada.append('diaria', document.getElementById('form_pou_diaria').value);
        pousada.append('tamanho', document.getElementById('form_pou_tamanho').value);
        if(document.getElementById('form_pou_piscina').checked){
            pousada.append('piscina', 1);
        }else{
            pousada.append('piscina', 0);
        }
        pousada.append('telefone', document.getElementById('form_pou_telefone').value);
        pousada.append('descricao', $('#form_pou_descricao').val());
        pousada.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('input_imgDestaque').files.length != 0){
            pousada.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            pousada.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var pos_array;
        var table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                pousada.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                pousada.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }
            i++;
        }
        pousada.append('tamanho_galeria', tam_uploaded);
        $.ajax({
            type:"POST",
            url:'pousadas',
            data:pousada,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success:function(msg){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = msg;
                exibir_snackbar('snackbar2');
                if(msg == 'Pousada cadastrada com sucesso'){
                    fetch_data_pousadas();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function atualizar_pousada(){
    if(form_pou_valido()){
        pousada = new FormData();
        pousada.append('id', document.getElementById('form_pou_id').value);
        pousada.append('nome', document.getElementById('form_pou_nome').value);
        pousada.append('endereco', document.getElementById('form_pou_endereco').value);
        pousada.append('diaria', document.getElementById('form_pou_diaria').value);
        pousada.append('tamanho', document.getElementById('form_pou_tamanho').value);
        if(document.getElementById('form_pou_piscina').checked){
            pousada.append('piscina', 1);
        }else{
            pousada.append('piscina', 0);
        }
        pousada.append('telefone', document.getElementById('form_pou_telefone').value);
        pousada.append('descricao', $('#form_pou_descricao').val());
        pousada.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('input_imgDestaque').files.length != 0){
            pousada.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            pousada.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var tam_deleted = 0;
        var table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0' && table[i].cells[3].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                pousada.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                pousada.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }else{
                if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '1' && table[i].cells[3].textContent == '1'){
                    pousada.append('deletar_galeria_foto_'+tam_deleted, table[i].cells[4].textContent);
                    tam_deleted++;
                    table[i].cells[1].textContent = "";
                    table[i].cells[2].textContent = "";
                    table[i].cells[3].textContent = "";
                    table[i].cells[4].textContent = "";
                    table[i].cells[5].textContent = "";
                    table[i].cells[6].textContent = "";
                }
            }
            i++;
        }
        pousada.append('tamanho_galeria', tam_uploaded);
        pousada.append('tamanho_deletar_galeria', tam_deleted);
        $.ajax({
            type: 'POST',
            url: 'pousadas/update',
            data: pousada,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success: function(array){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = array.msg;
                exibir_snackbar('snackbar2');
                if(array.msg == 'Pousada atualizada com sucesso'){
                    table = $('#table_galeria>tbody>tr');
                    i = 0;
                    while(i < table.length){
                        if(table[i].cells[3].textContent == '0'){
                            document.getElementById('galeria_div_'+i).remove();
                            table[i].remove();
                        }
                        i++;
                    }
                    galeriaFileArray.length = 0;
                    i = 0;
                    while(i < array.novas_fotos.length){
                        galeriaImgPreview(array.novas_fotos[i].url_imagem, array.novas_fotos[i], 0, 0);
                        i++;
                    }
                    ocultar_galeria();
                    fetch_data_pousadas();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function fetch_data_pousadas(){
    $('#pou_autocomplete').fadeOut();
    page = document.getElementById('paginacao_pousadas').value;
    input = document.getElementById('pou_search').value;
    select = document.getElementById('pou_select').value;
    $.ajax({
        url:"/pousadas/fetch_data?page="+page+"&input="+input+"&select="+select,
        beforeSend: function(){
            document.getElementById('tabela_pousadas').innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/icons/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById('tabela_pousadas').innerHTML = data;
        }
    });
}

$('#pou_search').keyup(function(){
    input = $(this).val();
    select = document.getElementById('pou_select').value;
    if (event.keyCode === 13) {
        fetch_data_pousadas();
    }else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('pousadas/autocomplete', search, function(pousadas){
                document.getElementById('pou_autocomplete').innerHTML ='';
                if(pousadas.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#pou_autocomplete').append(data);
                    pousadas.forEach(function(pousadas){
                        data = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\">"
                                    +"<img class= 'rounded img-autocomplete' src='"+pousadas.imagem_destaque+"'> "
                                    +"<strong>Nome:</strong> "
                                    +pousadas.nome+" <strong>Endereço: </strong>"
                                    +pousadas.endereco+" <strong>Tamanho da Suíte: </strong>"
                                    +pousadas.tamanho+"</button></li>");
                        data.find('button').click(function() {
                            preenche_pou_input(pousadas);
                        });
                        $('#pou_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#pou_autocomplete').append(data);
                    $('#pou_autocomplete').fadeIn();
                }
            });
        }
    }
});

function preenche_pou_input(pousada){
    select = document.getElementById('pou_select').value;
    field = pousada.nome;
    if(select.includes('endereco')){
        field = pousada.endereco;
    }
    if(select.includes('tamanho')){
        field = pousada.tamanho;
    }
    document.getElementById('pou_search').value = field;
    $('#pou_autocomplete').fadeOut();
}

function open_confirm_pou(pousada, option){
    document.getElementById('confirm_pou_id').value = pousada.id;
    if(option){
        document.getElementById('confirm_pou_msg').innerHTML = "Você tem certeza que deseja desabilitar a pousada "+pousada.nome+" ?";
        document.getElementById('confirm_pou_title').innerHTML = 'Desabilitar Pousada';
        document.getElementById('confirm_pou_button').innerHTML = '<i class="fas fa-minus-circle text-white"></i> Desabilitar';
    }else{
        document.getElementById('confirm_pou_msg').innerHTML = "Você tem certeza que deseja habilitar a pousada "+pousada.nome+" ?";
        document.getElementById('confirm_pou_title').innerHTML = 'Habilitar Pousada';
        document.getElementById('confirm_pou_button').innerHTML = '<i class="fas fa-check-circle"></i> Habilitar';
    }
    $('#confirm_pou_modal').modal({backdrop: 'static', keyboard: false});
}

function action_confirm_pou(){
    var button = document.getElementById('confirm_pou_button').innerHTML;
    if(button == '<i class="fas fa-minus-circle text-white"></i> Desabilitar'){
        desabilitar_pousada();
    }else{
        habilitar_pousada();
    }
}

function desabilitar_pousada(){
    id = document.getElementById('confirm_pou_id').value;
    $.ajax({
        type: 'DELETE',
        url: 'pousadas/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Pousada desabilitada com sucesso'){
                fetch_data_pousadas();
            }
        }
    });
}

function habilitar_pousada(){
    id = document.getElementById('confirm_pou_id').value;
    $.ajax({
        type: 'GET',
        url: 'pousadas/create/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Pousada habilitada com sucesso'){
                fetch_data_pousadas();
            }
        }
    });
}

function open_modal_pousada_details(obj, galeria){
    document.getElementById('details_pou_img').src = '../'+obj.imagem_destaque;
    document.getElementById('details_pou_img_link').href = '../'+obj.imagem_destaque;
    document.getElementById('details_pou_nome').innerHTML = obj.nome;
    document.getElementById('details_pou_endereco').innerHTML = '<i class="fas fa-map-marker-alt"></i> <strong>Endereço: </strong>'+ obj.endereco;
    document.getElementById('details_pou_telefone').innerHTML = '<i class="fas fa-phone"></i> <strong>Telefone: </strong>' + obj.telefone;
    document.getElementById('details_pou_piscina').innerHTML = (obj.piscina ? '<i class="far fa-check-square"></i>' : '<i class="far fa-square"></i>') + "<strong> Possui piscina</strong>"; 
    document.getElementById('details_pou_tamanho').innerHTML = '<strong>Quartos: </strong>'+obj.tamanho;
    document.getElementById('details_pou_diaria').innerHTML = '<strong>Diária: </strong>'+obj.diaria;
    document.getElementById('details_pou_descricao').innerHTML = obj.descricao;
    $('#modal_pousada_details').modal();
  
    var i = 0;
    img_viewer_control = 0;
    document.getElementById('galeria_divContent').innerHTML = "";
    while(i < galeria.length){
        galeriaImgPreview('../'+galeria[i].url_imagem, galeria[i], 0, 1);
        i++;
    }    
}
