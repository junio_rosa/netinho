function open_form_p4x4(obj, galeria, datas_marcadas){
    galeriaFileArray.length = 0;
    img_viewer_control = 0;
    datas_bd.length = 0;
    datas_para_deletar.length = 0;
    datas_para_upload.length = 0;
    if(obj == 0){
        var array_input = ['form_p4x4_nome'];
        var array_small = [
            {
                small: 'small_px4_nome',
                msg: 'Digite o nome ou motorista do passeio'
            }
        ];
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        $('#form_p4x4_descricao').jqteVal("");
        $('#form_p4x4_horarios').jqteVal("");
        limpar_campos_formulario(array_input, array_small);
        document.getElementById('imgDestaque').src = "assets/icons/sem_img.png";
        document.getElementById('imgDestaqueLink').href = "assets/icons/sem_img.png";
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_p4x4_title').innerHTML = 'Adicionar Passeio 4x4';
        document.getElementById('form_p4x4_button').innerHTML = '<i class="fas fa-plus-circle text-white"></i> Cadastrar';
    }else{
        var i = 0;
        while(i < datas_marcadas.length){
            datas_bd.push(datas_marcadas[i].data_aluguel);
            i++;
        }
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        document.getElementById('form_p4x4_title').innerHTML = 'Editar Passeio 4x4';
        document.getElementById('form_p4x4_button').innerHTML = '<i class="fas fa-sync"></i> Atualizar';
        document.getElementById('imgDestaque').src = obj.imagem_destaque;
        document.getElementById('imgDestaqueLink').href = obj.imagem_destaque;
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_p4x4_id').value = obj.id;
        document.getElementById('form_p4x4_nome').value = obj.nome;
        $('#form_p4x4_descricao').jqteVal(obj.descricao);
        $('#form_p4x4_horarios').jqteVal(obj.horarios);
        validacao_campo('form_p4x4_nome', 'small_px4_nome', 'Campo Validado', 'Nome inválido', 1);
        i = 0;
        while(i < galeria.length){
            galeriaImgPreview(galeria[i].url_imagem, galeria[i], 0, 0);
            i++;
        }
    }
    var data_atual = new Date();
    montar_calendario(data_atual, 0);
    $('#form_p4x4_modal').modal({backdrop: 'static', keyboard: false});
}

function form_4x4_valido(){
    validacao_campo('form_p4x4_nome', 'small_px4_nome', 'Campo Validado', 'Nome inválido', 1);
    var control = true;
    var nome = document.getElementById('small_px4_nome').innerHTML;
    var horarios = document.getElementById('form_p4x4_horarios').value;
    var descricao = document.getElementById('form_p4x4_descricao').value;
    var imagem_destaque = document.getElementById('input_imgDestaque').files;
    var msg_erro = '';
    if(nome != 'Campo Validado'){
        msg_erro += nome+'<br>';
    }
    if(horarios.trim() == ''){
        msg_erro += 'Campo horários vazio<br>';
    }
    if(descricao.trim() == ''){
        msg_erro += 'Campo descrição vazio<br>';
    }
    if(imagem_destaque.length != 0){
        if(!imagem_destaque[0].type.includes('image')){
            msg_erro += 'Formato de imagem inválido para upload<br>';
        }
    }
    if(msg_erro != ''){
        document.getElementById('snackbar2').innerHTML = 'ERROS:<br><br>'+msg_erro;
        exibir_snackbar('snackbar2');
        control = false;
    }
    return control;
}

function action_form_p4x4(){
    var button = document.getElementById('form_p4x4_button').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-white"></i> Cadastrar'){
        cadastrar_passeio4x4();
    }else{
        atualizar_passeio4x4();
    }
}

function cadastrar_passeio4x4(){
    if(form_4x4_valido()){
        p4x4 = new FormData();
        p4x4.append('nome', document.getElementById('form_p4x4_nome').value);
        p4x4.append('horarios', $('#form_p4x4_horarios').val());
        p4x4.append('descricao', $('#form_p4x4_descricao').val());
        p4x4.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('input_imgDestaque').files.length != 0){
            p4x4.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            p4x4.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var pos_array;
        var table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                p4x4.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                p4x4.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }
            i++;
        }
        p4x4.append('tamanho_galeria', tam_uploaded);
        p4x4.append('datas_para_upload', datas_para_upload);
        $.ajax({
            type:"POST",
            url:'passeio4x4',
            data:p4x4,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success:function(msg){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = msg;
                exibir_snackbar('snackbar2');
                if(msg == 'Passeio 4x4 cadastrado com sucesso'){
                    fetch_data_passeios_4x4();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function atualizar_passeio4x4(){
    console.log($('#form_p4x4_descricao').val());
    if(form_4x4_valido()){
        passeio4x4 = new FormData();
        passeio4x4.append('id', document.getElementById('form_p4x4_id').value);
        passeio4x4.append('nome', document.getElementById('form_p4x4_nome').value);
        passeio4x4.append('horarios', $('#form_p4x4_horarios').val());
        passeio4x4.append('descricao', $('#form_p4x4_descricao').val());
        passeio4x4.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('input_imgDestaque').files.length != 0){
            passeio4x4.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            passeio4x4.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var tam_deleted = 0;
        table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0' && table[i].cells[3].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                passeio4x4.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                passeio4x4.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }else{
                if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '1' && table[i].cells[3].textContent == '1'){
                    passeio4x4.append('deletar_galeria_foto_'+tam_deleted, table[i].cells[4].textContent);
                    tam_deleted++;
                    table[i].cells[1].textContent = "";
                    table[i].cells[2].textContent = "";
                    table[i].cells[3].textContent = "";
                    table[i].cells[4].textContent = "";
                    table[i].cells[5].textContent = "";
                    table[i].cells[6].textContent = "";
                }
            }
            i++;
        }
        passeio4x4.append('tamanho_galeria', tam_uploaded);
        passeio4x4.append('tamanho_deletar_galeria', tam_deleted);
        passeio4x4.append('datas_para_upload', datas_para_upload);
        passeio4x4.append('datas_para_deletar', datas_para_deletar);
        $.ajax({
            type: 'POST',
            url: 'passeio4x4/update',
            data: passeio4x4,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success: function(array){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = array.msg;
                exibir_snackbar('snackbar2');
                if(array.msg == 'Passeio 4x4 atualizado com sucesso'){
                    i = 0;
                    datas_bd.length = 0;
                    while(i < array.datas_bd.length){
                        datas_bd.push(array.datas_bd[i].data_aluguel);
                        i++;
                    }
                    datas_para_deletar.length = 0;
                    datas_para_upload.length = 0;
                    var data_atual = new Date();
                    montar_calendario(data_atual, 0);

                    table = $('#table_galeria>tbody>tr');
                    i = 0;
                    while(i < table.length){
                        if(table[i].cells[3].textContent == '0'){
                            document.getElementById('galeria_div_'+i).remove();
                            table[i].remove();
                        }
                        i++;
                    }
                    galeriaFileArray.length = 0;
                    i = 0;
                    while(i < array.novas_fotos.length){
                        galeriaImgPreview(array.novas_fotos[i].url_imagem, array.novas_fotos[i], 0, 0);
                        i++;
                    }
                    ocultar_galeria();
                    fetch_data_passeios_4x4();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function fetch_data_passeios_4x4(){
    $('#p4x4_autocomplete').fadeOut();
    page = document.getElementById('paginacao_p4x4').value;
    input = document.getElementById('p4x4_search').value;
    select = document.getElementById('p4x4_select').value;
    $.ajax({
        url:"/passeio4x4/fetch_data?page="+page+"&input="+input+"&select="+select,
        beforeSend: function(){
            document.getElementById('tabela_passeios_4x4').innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/icons/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById('tabela_passeios_4x4').innerHTML = data;
        }
    });
}

$('#p4x4_search').keyup(function(){
    input = $(this).val();
    select = document.getElementById('p4x4_select').value;
    if (event.keyCode === 13) {
        fetch_data_passeios_4x4();
    }else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('passeio4x4/autocomplete', search, function(passeio4x4){
                document.getElementById('p4x4_autocomplete').innerHTML ='';
                if(passeio4x4.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#p4x4_autocomplete').append(data);
                    passeio4x4.forEach(function(passeio4x4){
                        passeio4x4 = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\">"
                                    +"<img class= 'rounded img-autocomplete' src='"+passeio4x4.imagem_destaque+"'> "
                                    +"<strong>Nome:</strong> "
                                    +passeio4x4.nome+" </button></li>");
                        passeio4x4.find('button').click(function() {
                            preenche_p4x4_input(passeio4x4);
                        });
                        $('#p4x4_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#p4x4_autocomplete').append(data);
                    $('#p4x4_autocomplete').fadeIn();
                }
            });
        }
    }
});

function preenche_p4x4_input(passeio4x4){
    document.getElementById('p4x4_search').value = passeio4x4.nome;
    $('#p4x4_autocomplete').fadeOut();
}

function open_confirm_p4x4(passeio4x4, option){
    document.getElementById('confirm_p4x4_id').value = passeio4x4.id;
    if(option){
        document.getElementById('confirm_p4x4_msg').innerHTML = "Você tem certeza que deseja desabilitar o passeio 4x4 "+passeio4x4.nome+" ?";
        document.getElementById('confirm_p4x4_title').innerHTML = 'Desabilitar Passeio 4x4';
        document.getElementById('confirm_p4x4_button').innerHTML = '<i class="fas fa-minus-circle text-white"></i> Desabilitar';
    }else{
        document.getElementById('confirm_p4x4_msg').innerHTML = "Você tem certeza que deseja habilitar o passeio 4x4 "+passeio4x4.nome+" ?";
        document.getElementById('confirm_p4x4_title').innerHTML = 'Habilitar Passeio 4x4';
        document.getElementById('confirm_p4x4_button').innerHTML = '<i class="fas fa-check-circle"></i> Habilitar';
    }
    $('#confirm_p4x4_modal').modal({backdrop: 'static', keyboard: false});
}

function action_confirm_p4x4(){
    var button = document.getElementById('confirm_p4x4_button').innerHTML;
    if(button == '<i class="fas fa-minus-circle text-white"></i> Desabilitar'){
        desabilitar_passeio4x4();
    }else{
        habilitar_passeio4x4();
    }
}

function desabilitar_passeio4x4(){
    id = document.getElementById('confirm_p4x4_id').value;
    $.ajax({
        type: 'DELETE',
        url: 'passeio4x4/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Passeio 4x4 desabilitado com sucesso'){
                fetch_data_passeios_4x4();
            }
        }
    });
}

function habilitar_passeio4x4(){
    id = document.getElementById('confirm_p4x4_id').value;
    $.ajax({
        type: 'GET',
        url: 'passeio4x4/create/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Passeio 4x4 habilitado com sucesso'){
                fetch_data_passeios_4x4();
            }
        }
    });
}

function open_details_passeio4x4(obj, galeria, calendario){
    document.getElementById('details_p4x4_img').src = obj.imagem_destaque;
    document.getElementById('details_p4x4_img_link').href = obj.imagem_destaque;
    document.getElementById('details_4x4_horarios').innerHTML = obj.horarios;
    document.getElementById('details_4x4_descricao').innerHTML = obj.descricao;

    var i = 0;
    img_viewer_control = 0;
    document.getElementById('galeria_divContent').innerHTML = "";
    while(i < galeria.length){
        galeriaImgPreview(galeria[i].url_imagem, galeria[i], 0, 1);
        i++;
    }
    i = 0;
    while(i < calendario.length){
        datas_bd.push(calendario[i].data_aluguel);
        i++;
    }
    montar_calendario(new Date(), 1);
    $('#modal_p4x4_details').modal();
}

