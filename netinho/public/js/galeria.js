/*======================================INPUT FILE========================================*/
$('#imgDestaque').click(function(){
    $("#input_imgDestaque").click();
});

$('#inputGaleria').on('change',function(e){
    var files = e.target.files;
    $.each(files, function(i, file){
        var image = 1;
        if(!file.type.includes('image')){
            image = 0;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(e){
            if(image){
                image = e.target.result;
            }else{
                image = 'assets/icons/imagem_nao_suportada.png';
            }
            galeriaImgPreview(image, file, 1, 0);
        };
    });
});
/*======================================INPUT TEXT========================================*/

/*=======================================GALERIA==========================================*/
function galeriaImgPreview(img, obj, option, is_home_page){
    var table = $('#table_galeria>tbody>tr');
    var tamanho_tabela = table.length;
    if(!option){
        $('#table_galeria>tbody').append(linha_tabela_controle(tamanho_tabela, obj.url_imagem, 'image', 1, obj.id, "", 0));    
    }else{
        $('#table_galeria>tbody').append(linha_tabela_controle(tamanho_tabela, obj.name, obj.type, 0, "", galeriaFileArray.length, 0));
        galeriaFileArray.push(obj);
    }
    if(is_home_page){
        var template = $("<div class=\"col-12 col-sm-6 col-md-4 col-lg-3 my-2 d-none\" id=\"galeria_div_"+tamanho_tabela+"\">" +
                            "<a id=\"galeria_link_"+tamanho_tabela+"\" class=\"galeria-preview-thumb\" rel=\"galeria-preview-thumb\" href=\""+img+"\" title=\"Galeria Preview "+(tamanho_tabela + 1)+"\">"+
                                '<img class="card-img cursor-pointer" src="'+img+'">'+
                            "</a>"+
                        '</div>');
        template.find('img').click(function(){
            show_img_fullscreen();
        });
    }else{
        var template = $("<div class=\"col-12 col-sm-6 col-md-4 col-lg-3 d-none\" id=\"galeria_div_"+tamanho_tabela+"\">" +
                            '<div class="card my-3">'+
                                '<div class="card-title">'+
                                    "<a id=\"galeria_link_"+tamanho_tabela+"\" class=\"galeria-preview-thumb\" rel=\"galeria-preview-thumb\" href=\""+img+"\" title=\"Galeria Preview "+(tamanho_tabela + 1)+"\">"+
                                        '<img class="card-img cursor-pointer" src="'+img+'">'+
                                    "</a>"+
                                '</div>'+
                                '<div class="card-body text-center">'+
                                    '<button class="btn btn-sm btn-secondary" type="button">'+
                                        '<i class="far fa-trash-alt"></i> Remover'+
                                    '</button>'+
                                '</div>'+
                            '</div>'+
                        '</div>');
        template.find('img').click(function(){
            show_img_fullscreen();
        });
        template.find('button').click(function() {
            removerImagem(tamanho_tabela);
        });
    }
    $('#galeria_divContent').append(template);
}

function show_img_fullscreen(){
    $(".galeria-preview-thumb").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
		prevEffect	: 'fade',
		nextEffect	: 'fade',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
    });
}
/*=======================================GALERIA==========================================*/

/*=======================================CONTROL==========================================*/
function linha_tabela_controle(codigo, nome_arquivo, extensao, uploaded, id_bd, pos_array_file, deletado){
    var linha = "<tr class=\"text-center\">"
                    +"<td class='align-middle'>"+codigo+"</td>"
                    +"<td class='align-middle'>"+nome_arquivo+"</td>"
                    +"<td class='align-middle'>"+extensao+"</td>"
                    +"<td class='align-middle'>"+uploaded+"</td>"
                    +"<td class='align-middle'>"+id_bd+"</td>"
                    +"<td class='align-middle'>"+pos_array_file+"</td>"
                    +"<td class='align-middle'>"+deletado+"</td>"
                +"</tr>";
    return linha;
}

function removerImagem(pos_tabela){
    var table = $('#table_galeria>tbody>tr');
    table[pos_tabela].cells[6].textContent = 1;
    document.getElementById('galeria_div_'+pos_tabela).classList.add('d-none');
    document.getElementById('galeria_link_'+pos_tabela).classList.remove('galeria-preview-thumb');
    document.getElementById('snackbar2').innerHTML = 'Imagem removida com sucesso';
    exibir_snackbar('snackbar2');
}

function visualizar_mais(){
    var table = $('#table_galeria>tbody>tr');
    if(table.length == 0){
        document.getElementById('snackbar2').innerHTML = 'Galeria de imagens vazia';
        exibir_snackbar('snackbar2');
    }else{
        if(img_viewer_control == table.length){
            document.getElementById('snackbar2').innerHTML = 'Todas as imagens já estão abertas';
            exibir_snackbar('snackbar2');
        }else{
            var limite = img_viewer_control + 12;
            while(img_viewer_control < limite && img_viewer_control < table.length){
                if(table[img_viewer_control].cells[6].textContent == '0'){
                    document.getElementById('galeria_div_'+img_viewer_control).classList.remove('d-none');
                }else{
                    limite++;
                }
                img_viewer_control++;
            }
        }
    }
}

function visualizar_menos(){
    var table = $('#table_galeria>tbody>tr');
    if(table.length == 0){
        document.getElementById('snackbar2').innerHTML = 'Galeria de imagens vazia';
        exibir_snackbar('snackbar2');
    }else{
        if(img_viewer_control == 0){
            document.getElementById('snackbar2').innerHTML = 'Todas as imagens já estão ocultadas';
            exibir_snackbar('snackbar2');
        }else{
            var limite = img_viewer_control - 12;
            while(img_viewer_control > limite && img_viewer_control > 0){
                img_viewer_control--;
                if(table[img_viewer_control].cells[6].textContent == '0'){
                    document.getElementById('galeria_div_'+img_viewer_control).classList.add('d-none');
                }else{
                    --limite;
                }
            }
        }
    }
}

function ocultar_galeria(){
    img_viewer_control = 0;
    var table = $('#table_galeria>tbody>tr');
    while(img_viewer_control < table.length){
        document.getElementById('galeria_div_'+img_viewer_control).classList.add('d-none');
        img_viewer_control++;
    }
    img_viewer_control = 0;
}
/*=======================================CONTROL==========================================*/
