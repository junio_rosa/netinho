function open_form_lancha(obj, galeria, datas_marcadas){
    galeriaFileArray.length = 0;
    img_viewer_control = 0;
    datas_bd.length = 0;
    datas_para_deletar.length = 0;
    datas_para_upload.length = 0;
    if(obj == 0){
        var array_input = ['form_lancha_nome'];
        var array_small = [
            {
                small: 'small_lancha_nome',
                msg: 'Digite o nome ou motorista do passeio'
            }
        ];
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        $('#form_lancha_descricao').jqteVal("");
        $('#form_lancha_horarios').jqteVal("");
        limpar_campos_formulario(array_input, array_small);
        document.getElementById('imgDestaque').src = "assets/icons/sem_img.png";
        document.getElementById('imgDestaqueLink').href = "assets/icons/sem_img.png";
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_lancha_title').innerHTML = 'Adicionar Passeio Lancha';
        document.getElementById('form_lancha_button').innerHTML = '<i class="fas fa-plus-circle text-white"></i> Cadastrar';
    }else{
        var i = 0;
        while(i < datas_marcadas.length){
            datas_bd.push(datas_marcadas[i].data_aluguel);
            i++;
        }
        document.getElementById('galeria_divContent').innerHTML = "";
        $('#table_galeria>tbody>tr').remove();
        document.getElementById('form_lancha_title').innerHTML = 'Editar Passeio Lancha';
        document.getElementById('form_lancha_button').innerHTML = '<i class="fas fa-sync"></i> Atualizar';
        document.getElementById('imgDestaque').src = obj.imagem_destaque;
        document.getElementById('imgDestaqueLink').href = obj.imagem_destaque;
        document.getElementById('input_imgDestaque').value = '';
        document.getElementById('form_lancha_id').value = obj.id;
        document.getElementById('form_lancha_nome').value = obj.nome;
        $('#form_lancha_descricao').jqteVal(obj.descricao);
        $('#form_lancha_horarios').jqteVal(obj.horarios);
        validacao_campo('form_lancha_nome', 'small_lancha_nome', 'Campo Validado', 'Nome inválido', 1);
        i = 0;
        while(i < galeria.length){
            galeriaImgPreview(galeria[i].url_imagem, galeria[i], 0, 0);
            i++;
        }
    }
    var data_atual = new Date();
    montar_calendario(data_atual, 0);
    $('#form_lancha_modal').modal({backdrop: 'static', keyboard: false});
}

function form_lancha_valido(){
    validacao_campo('form_lancha_nome', 'small_lancha_nome', 'Campo Validado', 'Nome inválido', 1);
    var control = true;
    var nome = document.getElementById('small_lancha_nome').innerHTML;
    var horarios = document.getElementById('form_lancha_horarios').value;
    var descricao = document.getElementById('form_lancha_descricao').value;
    var imagem_destaque = document.getElementById('input_imgDestaque').files;
    var msg_erro = '';
    if(nome != 'Campo Validado'){
        msg_erro += nome+'<br>';
    }
    if(horarios.trim() == ''){
        msg_erro += 'Campo horários vazio<br>';
    }
    if(descricao.trim() == ''){
        msg_erro += 'Campo descrição vazio<br>';
    }
    if(imagem_destaque.length != 0){
        if(!imagem_destaque[0].type.includes('image')){
            msg_erro += 'Formato de imagem inválido para upload<br>';
        }
    }
    if(msg_erro != ''){
        document.getElementById('snackbar2').innerHTML = 'ERROS:<br><br>'+msg_erro;
        exibir_snackbar('snackbar2');
        control = false;
    }
    return control;
}

function action_form_lancha(){
    var button = document.getElementById('form_lancha_button').innerHTML;
    if(button == '<i class="fas fa-plus-circle text-white"></i> Cadastrar'){
        cadastrar_passeio_lancha();
    }else{
        atualizar_passeio_lancha();
    }
}

function cadastrar_passeio_lancha(){
    if(form_lancha_valido()){
        passeio_lancha = new FormData();
        passeio_lancha.append('nome', document.getElementById('form_lancha_nome').value);
        passeio_lancha.append('horarios', $('#form_lancha_horarios').val());
        passeio_lancha.append('descricao', $('#form_lancha_descricao').val());
        passeio_lancha.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('input_imgDestaque').files.length != 0){
            passeio_lancha.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            passeio_lancha.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var pos_array;
        var table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                passeio_lancha.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                passeio_lancha.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }
            i++;
        }
        passeio_lancha.append('tamanho_galeria', tam_uploaded);
        passeio_lancha.append('datas_para_upload', datas_para_upload);
        $.ajax({
            type:"POST",
            url:'passeio_lancha',
            data:passeio_lancha,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success:function(msg){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = msg;
                exibir_snackbar('snackbar2');
                if(msg == 'Passeio lancha cadastrado com sucesso'){
                    fetch_data_passeios_lancha();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function atualizar_passeio_lancha(){
    if(form_lancha_valido()){
        passeio_lancha = new FormData();
        passeio_lancha.append('id', document.getElementById('form_lancha_id').value);
        passeio_lancha.append('nome', document.getElementById('form_lancha_nome').value);
        passeio_lancha.append('horarios', $('#form_lancha_horarios').val());
        passeio_lancha.append('descricao', $('#form_lancha_descricao').val());
        passeio_lancha.append('imagem_destaque', document.getElementById('input_imgDestaque').files[0]);
        if(document.getElementById('input_imgDestaque').files.length != 0){
            passeio_lancha.append('img_destaque_extensao', document.getElementById('input_imgDestaque').files[0].type.replace('image/', ''));
        }else{
            passeio_lancha.append('img_destaque_extensao', "");
        }
        var i = 0;
        var tam_uploaded = 0;
        var tam_deleted = 0;
        table = $('#table_galeria>tbody>tr');
        while(i < table.length){
            if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '0' && table[i].cells[3].textContent == '0'){
                pos_array = parseInt(table[i].cells[5].textContent);
                passeio_lancha.append('galeria_foto_'+tam_uploaded, galeriaFileArray[pos_array]);
                passeio_lancha.append('galeria_extensao_'+tam_uploaded, table[i].cells[2].textContent.replace('image/', ''));
                tam_uploaded++;
            }else{
                if(table[i].cells[2].textContent.includes('image') && table[i].cells[6].textContent == '1' && table[i].cells[3].textContent == '1'){
                    passeio_lancha.append('deletar_galeria_foto_'+tam_deleted, table[i].cells[4].textContent);
                    tam_deleted++;
                    table[i].cells[1].textContent = "";
                    table[i].cells[2].textContent = "";
                    table[i].cells[3].textContent = "";
                    table[i].cells[4].textContent = "";
                    table[i].cells[5].textContent = "";
                    table[i].cells[6].textContent = "";
                }
            }
            i++;
        }
        passeio_lancha.append('tamanho_galeria', tam_uploaded);
        passeio_lancha.append('tamanho_deletar_galeria', tam_deleted);
        passeio_lancha.append('datas_para_upload', datas_para_upload);
        passeio_lancha.append('datas_para_deletar', datas_para_deletar);
        $.ajax({
            type: 'POST',
            url: 'passeio_lancha/update',
            data: passeio_lancha,
            contentType:false,
            processData: false,
            beforeSend: function(){
                document.getElementById('snackbar2').className = "beforeSend";
                document.getElementById('snackbar2').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
            },
            success: function(array){
                document.getElementById('snackbar2').className.replace("beforeSend", "");
                document.getElementById('snackbar2').innerHTML = array.msg;
                exibir_snackbar('snackbar2');
                if(array.msg == 'Passeio lancha atualizado com sucesso'){
                    i = 0;
                    datas_bd.length = 0;
                    while(i < array.datas_bd.length){
                        datas_bd.push(array.datas_bd[i].data_aluguel);
                        i++;
                    }
                    datas_para_deletar.length = 0;
                    datas_para_upload.length = 0;
                    var data_atual = new Date();
                    montar_calendario(data_atual, 0);

                    table = $('#table_galeria>tbody>tr');
                    i = 0;
                    while(i < table.length){
                        if(table[i].cells[3].textContent == '0'){
                            document.getElementById('galeria_div_'+i).remove();
                            table[i].remove();
                        }
                        i++;
                    }
                    galeriaFileArray.length = 0;
                    i = 0;
                    while(i < array.novas_fotos.length){
                        galeriaImgPreview(array.novas_fotos[i].url_imagem, array.novas_fotos[i], 0, 0);
                        i++;
                    }
                    ocultar_galeria();
                    fetch_data_passeios_lancha();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function fetch_data_passeios_lancha(){
    $('#lancha_autocomplete').fadeOut();
    page = document.getElementById('paginacao_lancha').value;
    input = document.getElementById('lancha_search').value;
    select = document.getElementById('lancha_select').value;
    $.ajax({
        url:"/passeio_lancha/fetch_data?page="+page+"&input="+input+"&select="+select,
        beforeSend: function(){
            document.getElementById('tabela_passeios_lancha').innerHTML = '<div class="card-body w-100">'
                + '<div class="table-responsive text-center">'
                + '<img src="assets/icons/loading.gif">'
                + '</div>'
                + '</div>';
        },
        success:function(data){
            document.getElementById('tabela_passeios_lancha').innerHTML = data;
        }
    });
}

$('#lancha_search').keyup(function(){
    input = $(this).val();
    select = document.getElementById('lancha_select').value;
    if (event.keyCode === 13) {
        fetch_data_passeios_lancha();
    }else{
        if(input != ''){
            search = {
                input: input,
                select: select
            }
            $.post('passeio_lancha/autocomplete', search, function(passeio_lancha){
                document.getElementById('lancha_autocomplete').innerHTML ='';
                if(passeio_lancha.length > 0){
                    var data = $('<ul class="list-group">');
                    $('#lancha_autocomplete').append(data);
                    passeio_lancha.forEach(function(passeio_lancha){
                        passeio_lancha = $("<li class='list-group'><button class='list-group-item autocomplete w-100 text-left' type=\"button\">"
                                    +"<img class= 'rounded img-autocomplete' src='"+passeio_lancha.imagem_destaque+"'> "
                                    +"<strong>Nome:</strong> "
                                    +passeio_lancha.nome+" </button></li>");
                        passeio_lancha.find('button').click(function() {
                            preenche_lancha_input(passeio_lancha);
                        });
                        $('#lancha_autocomplete').append(data);
                    });
                    data = $('</ul>');
                    $('#lancha_autocomplete').append(data);
                    $('#lancha_autocomplete').fadeIn();
                }
            });
        }
    }
});

function preenche_lancha_input(lancha){
    document.getElementById('lancha_search').value = lancha.nome;
    $('#lancha_autocomplete').fadeOut();
}

function open_confirm_lancha(passeio_lancha, option){
    document.getElementById('confirm_lancha_id').value = passeio_lancha.id;
    if(option){
        document.getElementById('confirm_lancha_msg').innerHTML = "Você tem certeza que deseja desabilitar o passeio lancha "+passeio_lancha.nome+" ?";
        document.getElementById('confirm_lancha_title').innerHTML = 'Desabilitar Passeio Lancha';
        document.getElementById('confirm_lancha_button').innerHTML = '<i class="fas fa-minus-circle text-white"></i> Desabilitar';
    }else{
        document.getElementById('confirm_lancha_msg').innerHTML = "Você tem certeza que deseja habilitar o passeio lancha "+passeio_lancha.nome+" ?";
        document.getElementById('confirm_lancha_title').innerHTML = 'Habilitar Passeio Lancha';
        document.getElementById('confirm_lancha_button').innerHTML = '<i class="fas fa-check-circle"></i> Habilitar';
    }
    $('#confirm_lancha_modal').modal({backdrop: 'static', keyboard: false});
}

function action_confirm_lancha(){
    var button = document.getElementById('confirm_lancha_button').innerHTML;
    if(button == '<i class="fas fa-minus-circle text-white"></i> Desabilitar'){
        desabilitar_passeio_lancha();
    }else{
        habilitar_passeio_lancha();
    }
}

function desabilitar_passeio_lancha(){
    id = document.getElementById('confirm_lancha_id').value;
    $.ajax({
        type: 'DELETE',
        url: 'passeio_lancha/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Passeio lancha desabilitado com sucesso'){
                fetch_data_passeios_lancha();
            }
        }
    });
}

function habilitar_passeio_lancha(){
    id = document.getElementById('confirm_lancha_id').value;
    $.ajax({
        type: 'GET',
        url: 'passeio_lancha/create/'+id,
        beforeSend: function(){
            document.getElementById('snackbar3').className = "beforeSend";
            document.getElementById('snackbar3').innerHTML = '<img class="img-autocomplete" src="assets/icons/loading.gif">';
        },
        success: function(msg){
            document.getElementById('snackbar3').className.replace("beforeSend", "");
            document.getElementById('snackbar3').innerHTML = msg;
            exibir_snackbar('snackbar3');
            if(msg == 'Passeio lancha habilitado com sucesso'){
                fetch_data_passeios_lancha();
            }
        }
    });
}

function open_details_passeio_lancha(obj, galeria, calendario){
    document.getElementById('details_lancha_img').src = obj.imagem_destaque;
    document.getElementById('details_lancha_img_link').href = obj.imagem_destaque;
    document.getElementById('details_lancha_horarios').innerHTML = obj.horarios;
    document.getElementById('details_lancha_descricao').innerHTML = obj.descricao;

    var i = 0;
    img_viewer_control = 0;
    document.getElementById('galeria_divContent').innerHTML = "";
    while(i < galeria.length){
        galeriaImgPreview(galeria[i].url_imagem, galeria[i], 0, 1);
        i++;
    }
    i = 0;
    while(i < calendario.length){
        datas_bd.push(calendario[i].data_aluguel);
        i++;
    }
    montar_calendario(new Date(), 1);
    $('#modal_p_lancha_details').modal();
}
