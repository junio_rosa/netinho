/*======================================INPUT TEXT========================================*/
function small_erro(small, msg_erro) {
    document.getElementById(small).classList.remove('text-secondary');
    document.getElementById(small).classList.remove('text-success');
    document.getElementById(small).classList.add('text-danger');
    document.getElementById(small).innerHTML = msg_erro;
}

function small_padrao(small, msg_padrao) {
    document.getElementById(small).classList.remove('text-danger');
    document.getElementById(small).classList.remove('text-success');
    document.getElementById(small).classList.add('text-secondary');
    document.getElementById(small).innerHTML = msg_padrao;
}

function small_acerto(small, msg_acerto) {
    document.getElementById(small).classList.remove('text-danger');
    document.getElementById(small).classList.remove('text-secondary');
    document.getElementById(small).classList.add('text-success');
    document.getElementById(small).innerHTML = msg_acerto;
}

function input_valido(input) {
    document.getElementById(input).classList.remove('is-invalid');
    document.getElementById(input).classList.add('is-valid');
}

function input_invalido(input) {
    document.getElementById(input).classList.remove('is-valid');
    document.getElementById(input).classList.add('is-invalid');
}

function input_padrao(input) {
    document.getElementById(input).classList.remove('is-valid');
    document.getElementById(input).classList.remove('is-invalid');
    document.getElementById(input).value = '';
}
/*======================================INPUT TEXT========================================*/

/*=====================================VALIDAÇÕES=========================================*/
function validacao_email(input, small_email, msg_acerto, msg_erro) {
    email = document.getElementById(input).value;
    if(!email.includes("@")){
        input_invalido(input);
        small_erro(small_email, msg_erro);
    }else{
        email = email.split('@');
        usuario = email[0];
        dominio = email[1];

        if ((usuario.length >= 1) &&
            (dominio.length >= 3) &&
            (usuario.search("@") == -1) &&
            (dominio.search("@") == -1) &&
            (usuario.search(" ") == -1) &&
            (dominio.search(" ") == -1) &&
            (dominio.search(".") != -1) &&
            (dominio.indexOf(".") >= 1) &&
            (dominio.lastIndexOf(".") < dominio.length - 1)) {
                input_valido(input);
                small_acerto(small_email, msg_acerto);
        } else {
            input_invalido(input);
            small_erro(small_email, msg_erro);
        }
    }
}

function validacao_campo(input, small, msg_acerto, msg_erro, tamanho_campo){
    var campo = document.getElementById(input).value;
    if(campo.length < tamanho_campo){
        small_erro(small, msg_erro);
        input_invalido(input);
    }else{
        small_acerto(small, msg_acerto);
        input_valido(input);
    }
}

function validacao_senhas(input, input2, small1, small2){
    senha = document.getElementById(input).value;
    repita_senha = document.getElementById(input2).value;
    if(senha.length < 8){
        input_invalido(input);
        small_erro(small1, 'A senha deve possuir no mínimo 8 caracteres');
        input_invalido(input2);
        small_erro(small2, 'A senha deve ser válida para verificar uma segunda vez');
    }else{
        if(senha != repita_senha){
            input_invalido(input2);
            small_erro(small2, 'As senhas não são iguais');
        }else{
            input_valido(input2);
            small_acerto(small2, 'As senhas são iguais');
        }
    }
}
/*=====================================VALIDAÇÕES=========================================*/

/*======================================PREVIEWS==========================================*/
function exibir_snackbar(snack_id) {
    var x = document.getElementById(snack_id);
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}

function preview_imagem_destaque() {
    if (this.files && this.files[0]) {
        var imagem = 1;
        if(!this.files[0].type.includes('image')){
            imagem = 0;
        }
        var obj = new FileReader();
        obj.onload = function (data) {
            if(imagem){
                document.getElementById('imgDestaque').src = data.target.result;
                document.getElementById('imgDestaqueLink').href = data.target.result;
            }else{
                document.getElementById('imgDestaque').src = 'assets/icons/imagem_nao_suportada.png';
                document.getElementById('imgDestaqueLink').src = 'assets/icons/imagem_nao_suportada.png';
            }
            document.getElementById('imgDestaque').style.display = "block";
        }
        obj.readAsDataURL(this.files[0]);
    }
}

function mostrar_password(input, button) {
    var x = document.getElementById(input);
    if (x.type === "password") {
        x.type = "text";
        document.getElementById(button).innerHTML = '<i class="fas fa-eye-slash"></i>';
    } else {
        x.type = "password";
        document.getElementById(button).innerHTML = '<i class="fas fa-eye"></i>';
    }
}
/*======================================PREVIEWS==========================================*/

/*====================================FORMULÁRIOS=========================================*/
function limpar_campos_formulario(array_input, array_small) {
    var i = 0;
    while (i < array_input.length) {
        input_padrao(array_input[i]);
        i++;
    }
    i = 0;
    while (i < array_small.length) {
        small_padrao(array_small[i].small, array_small[i].msg);
        i++;
    }
}

function select_on_change(nome_input, autocomplete){
    document.getElementById(nome_input).value = '';
    $('#'+autocomplete).fadeOut();
}
/*====================================FORMULÁRIOS=========================================*/
