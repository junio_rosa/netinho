<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CasaController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\Passeio4x4Controller;
use App\Http\Controllers\PasseioLanchaController;
use App\Http\Controllers\PousadaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GuestController::class, 'index']);

Auth::routes();
Route::get('register', [LoginController::class, 'login']);

Route::get('/home', [HomeController::class, 'index'])->name('home');
/* ATUALIZAR DADOS DE PERFIL */
Route::post('/atualizar_perfil', [HomeController::class, 'atualizar_perfil']);
Route::get('/atualizar_navbar', [HomeController::class, 'atualizar_navbar']);
/* POUSADAS */
Route::get('/pousadas', [PousadaController::class, 'index'])->middleware('auth');
Route::post('/pousadas', [PousadaController::class, 'store']);
Route::get('/pousadas/fetch_data', [PousadaController::class, 'show']);
Route::post('/pousadas/autocomplete', [PousadaController::class, 'autocomplete']);
Route::delete('/pousadas/{id}', [PousadaController::class, 'destroy']);
Route::get('/pousadas/create/{id}', [PousadaController::class, 'create']);
Route::post('/pousadas/update', [PousadaController::class, 'update']);
/* CASAS */
Route::get('/casas', [CasaController::class, 'index'])->middleware('auth');
Route::post('/casas', [CasaController::class, 'store']);
Route::get('/casas/fetch_data', [CasaController::class, 'show']);
Route::post('/casas/autocomplete', [CasaController::class, 'autocomplete']);
Route::delete('/casas/{id}', [CasaController::class, 'destroy']);
Route::get('/casas/create/{id}', [CasaController::class, 'create']);
Route::post('/casas/update', [CasaController::class, 'update']);
/* PASSEIO 4X4 */
Route::get('/passeio4x4', [Passeio4x4Controller::class, 'index'])->middleware('auth');
Route::post('/passeio4x4', [Passeio4x4Controller::class, 'store']);
Route::get('/passeio4x4/fetch_data', [Passeio4x4Controller::class, 'show']);
Route::post('/passeio4x4/autocomplete', [Passeio4x4Controller::class, 'autocomplete']);
Route::delete('/passeio4x4/{id}', [Passeio4x4Controller::class, 'destroy']);
Route::get('/passeio4x4/create/{id}', [Passeio4x4Controller::class, 'create']);
Route::post('/passeio4x4/update', [Passeio4x4Controller::class, 'update']);
/* PASSEIO LANCHA */
Route::get('/passeio_lancha', [PasseioLanchaController::class, 'index'])->middleware('auth');
Route::post('/passeio_lancha', [PasseioLanchaController::class, 'store']);
Route::get('/passeio_lancha/fetch_data', [PasseioLanchaController::class, 'show']);
Route::post('/passeio_lancha/autocomplete', [PasseioLanchaController::class, 'autocomplete']);
Route::delete('/passeio_lancha/{id}', [PasseioLanchaController::class, 'destroy']);
Route::get('/passeio_lancha/create/{id}', [PasseioLanchaController::class, 'create']);
Route::post('/passeio_lancha/update', [PasseioLanchaController::class, 'update']);
/* HOME PAGE */
Route::get('/sobre_nos', [GuestController::class, 'sobre_nos']);
Route::get('/home/pousadas', [GuestController::class, 'index_pousadas']);
Route::get('/home/casas', [GuestController::class, 'index_casas']);
Route::get('/home/passeio4x4', [GuestController::class, 'index_passeio4x4']);
Route::get('/home/passeio_lancha', [GuestController::class, 'index_passeio_lancha']);
Route::get('/home_page/fetch_data', [GuestController::class, 'show']);
