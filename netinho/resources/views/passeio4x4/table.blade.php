<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$passeio4x4->count()}} passeios 4x4 de {{$passeio4x4->total()}} ({{$passeio4x4->firstItem()}} a {{$passeio4x4->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $passeio4x4->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Passeio 4x4</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($passeio4x4 as $p4x4)
                <tr class="text-center">
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='{{$p4x4->imagem_destaque}}'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong>{{$p4x4->nome}}<br>
                    </td>
                    <td class="align-middle">{{$p4x4->id}}</td>
                    <td class="align-middle">
                        @isset($able)
                            @if ($able)
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_p4x4({{$p4x4}}, {{$p4x4->galeria}}, {{$p4x4->calendario}})">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_p4x4({{$p4x4}}, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            @else
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_p4x4({{$p4x4}}, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            @endif
                        @endisset
                    </td>
                    @php
                        $i++;
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$passeio4x4->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>
