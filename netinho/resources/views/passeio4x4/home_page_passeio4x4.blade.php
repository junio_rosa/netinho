@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_guest')
@endsection

@section('content')
  <section id="passeio4x4_cards">
    @include('passeio4x4.guest_passeio4x4')
  </section>
@endsection

@section('javascript')
<script type="text/javascript">
    var img_viewer_control = 0;
    var datas_para_upload = new Array();
    var datas_para_deletar = new Array();
    var datas_bd = new Array();
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
        var calendario = {!! json_encode($search_result[0]->calendario) !!};
        var i = 0;
        while(i < calendario.length){
            datas_bd.push(calendario[i].data_aluguel);
            i++;
        }
        montar_calendario(new Date(), 1);
        var galeria = {!! json_encode($search_result[0]->galeria) !!};
        var img_destaque = {!! json_encode($search_result[0]->imagem_destaque) !!};
        i = 0;
        document.getElementById('galeria_divContent').innerHTML = "";
        galeriaImgPreview('../'+img_destaque, img_destaque, 0, 1);
        while(i < galeria.length){
            galeriaImgPreview('../'+galeria[i].url_imagem, galeria[i], 0, 1);
            i++;
        }
        visualizar_mais();
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page("passeio4x4", 'passeio4x4_cards');
    });
</script>
@endsection

