@component('mail::message')
<h3>Olá, {{$user->name}}</h3>
<div align="justify">
    Recentemente você fez uma requisição para redefinir senha na sua conta do site Resolve Escarpas.
    Sendo assim, clique no botão abaixo caso deseje redefini-la.
</div>

@component('mail::button', ['url' => url("password/reset/$token?email=$user->email")])
Redefinir Senha
@endcomponent

<div align="justify">
    Se você não fez essa requisição para redefinição de senha,
    por favor ignore esse e-mail ou responda, para o nosso suporte
    corrigir este erro. <br>
</div>
<br><br>

Atenciosamente,<br>
Suporte Resolve Escarpas.
@endcomponent
