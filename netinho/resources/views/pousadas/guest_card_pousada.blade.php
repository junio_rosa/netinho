<div class="container">
  <div class="form-row">
    @foreach($search_result as $pou)
    <div class="col-12 col-sm-6 col-md-4 col-lg-3 d-flex align-items-stretch">
        <div class="card my-3">
          <div class="card-title">
            <img class="card-img cursor-pointer" src="{{asset($pou->imagem_destaque)}}">
          </div>
          <div class="card-body text-center">
              <div class="col-12">
                <h3 class="font-weight-bold">
                  {{$pou->nome}}
                </h3>
              </div>
          </div>
          <div class="card-footer">
            <div class="col-12">
                <button class="btn btn-sm btn-info text-white" onclick="open_modal_pousada_details({{$pou}}, {{$pou->galeria}})">
                    <i class="fas fa-info-circle"></i> Detalhes
                </button>
            </div>
          </div>
        </div>
    </div>
    @endforeach

    <div class="col-12 w-100">
      {{$search_result->links("pagination::bootstrap-4")}}
    </div>

    <section>
        @include('pousadas.modal_detalhes')
    </section>
  </div>
</div>