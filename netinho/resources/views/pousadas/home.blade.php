@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_auth')
@endsection

@section('content')
<div class="container">
    <div class="card border my-4">
        <div class="card-header">
            <div class="container">
                <h4 class="card-title text-secondary">
                    Painel Administrativo de Pousadas
                </h4>
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="input-group" id="pou_search_group">
                            <input type="text" class="form-control"
                                    id="pou_search" placeholder="Busque por pousadas..."
                                    autocomplete="on">
                            <div class="input-group-append" id="pou_search_button">
                                <button class="btn btn-secondary rounded-right" type="button"
                                    onclick="fetch_data_pousadas()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>

                        <div id="pou_autocomplete" class="dropdown"></div>
                    </div>

                    <div class="form-group col-12">
                        <label>Selecione o tipo de busca:</label>
                        <select class='form-control w-100' id='pou_select' onchange="select_on_change('pou_search', 'pou_autocomplete')">
                            <option value='nome' selected='selected'>Buscar por Nome</option>
                            <option value='endereco'>Buscar por Endereço</option>
                            <option value='tamanho'>Buscar por Tamanho dos quartos</option>
                            <option value='nome_del'>Buscar por Nome (Pousadas desabilitadas)</option>
                            <option value='endereco_del'>Buscar por Endereço (Pousadas desabilitadas)</option>
                            <option value='tamanho_del'>Buscar por Tamanho dos quartos (Pousadas desabilitadas)</option>
                        </select>
                    </div>

                    <div class="form-group ml-auto">
                        <button class="btn btn-primary text-white" onclick="open_form_pou(0, '')">
                            <i class="fas fa-plus-circle"></i> Nova Pousada
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <section id="tabela_pousadas">
            @include('pousadas.table')
        </section>

        <input id="paginacao_pousadas" value="1" class="d-none">
    </div>
</div>

<section>
    @include('pousadas.modal_form')
</section>

<section>
    @include('pousadas.modal_confimacao')
</section>
@endsection

@section('javascript')
<script type="text/javascript">

    var galeriaFileArray = new Array();
    var img_viewer_control = 0;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    $(function(){
        var Phone_Mask_Behavior = function (val) {
            return val.replace(/\D/g, '').length == 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        Phone_Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(Phone_Mask_Behavior.apply({}, arguments), options);
            }
        };
        $('#form_pou_telefone').mask(Phone_Mask_Behavior, Phone_Options);
        $('.editor').jqte();
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        document.getElementById('paginacao_pousadas').value = pagina_atual;
        fetch_data_pousadas();
    });
</script>
@endsection
