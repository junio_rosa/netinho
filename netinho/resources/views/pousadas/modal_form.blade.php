<div class = "modal fade" id = "form_pou_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-xl" role = "document">
        <div class = "modal-content">

            <div class="modal-header bg-info">
                <h4 class="text-white" id="form_pou_title"></h4>
        
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    {{-- ID --}}
                    <input id="form_pou_id" class="d-none">
                    {{-- NOME --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Nome:</label>
                        <input class="form-control w-100" type="text" id="form_pou_nome" autocomplete="on"
                                onfocusout="validacao_campo('form_pou_nome', 'small_pou_nome', 'Campo Validado', 'Nome inválido', 1)">
                        <small id="small_pou_nome" class="text-secondary">
                            Digite o nome da pousada
                        </small>
                    </div>
                    {{-- ENDEREÇO --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Endereço:</label>
                        <input class="form-control w-100" type="text" id="form_pou_endereco" autocomplete="on"
                                onfocusout="validacao_campo('form_pou_endereco', 'small_pou_endereco', 'Campo Validado', 'Endereço inválido', 1)">
                        <small id="small_pou_endereco" class="text-secondary">
                            Digite o endereço da pousada
                        </small>
                    </div>
                    {{-- TAMANHO --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Tamanho dos quartos:</label>
                        <input class="form-control w-100" type="text" id="form_pou_tamanho" autocomplete="on"
                                onfocusout="validacao_campo('form_pou_tamanho', 'small_pou_tamanho', 'Campo Validado', 'Campo tamanho dos quartos inválido', 1)">
                        <small id="small_pou_tamanho" class="text-secondary">
                            Digite o tamanho e a quantidade de quartos
                        </small>
                    </div>
                    {{-- DIÁRIA --}}
                    <div class="form-group col-12 col-lg-6">
                        <label class="font-weight-bold">Diária:</label>
                        <input class="form-control w-100"
                                type="text"
                                id="form_pou_diaria" autocomplete="on"
                                onfocusout="validacao_campo('form_pou_diaria', 'small_pou_diaria', 'Campo Validado', 'Valor da diária inválido', 1)">
                        <small id="small_pou_diaria" class="text-secondary">
                            Digite o valor da diária
                        </small>
                    </div>
                    {{-- TELEFONE --}}
                    <div class="form-group col-12 col-lg-6">
                        <label class="font-weight-bold">Telefone:</label>
                        <input class="form-control w-100" type="text" id="form_pou_telefone" autocomplete="on"
                                onfocusout="validacao_campo('form_pou_telefone', 'small_pou_telefone', 'Campo Validado', 'Telefone inválido', 14)">
                        <small id="small_pou_telefone" class="text-secondary">
                            Digite o telefone para contato
                        </small>
                    </div>
                    {{-- PISCINA --}}
                    <div class="form-group col-12 text-left ml-4">
                        <input type="checkbox" class="form-check-input" id="form_pou_piscina">
                        <label class="font-weight-bold" for="form_pou_piscina">Possui piscina</label>
                    </div>
                    {{-- DESCRIÇÃO --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Descrição:</label>
                        <textarea class="form-control editor" type="text" id="form_pou_descricao"></textarea>
                    </div>

                    {{-- IMAGEM DESTAQUE --}}
                    <div class='form-group col-12'>
                        @include('helpers.img_destaque')
                    </div>
                    {{-- GALERIA --}}
                    <div class="form-group col-12">
                        @include('helpers.galeria')
                    </div>
                </div>

                <div id="snackbar2">Mensagem de erro:</div>
            </div>

            <div class="modal-footer">
                <div class="form-group col-12 text-right">
                    {{-- BUTTON REG UPG --}}
                    <button class="btn btn-primary mr-1" id="form_pou_button" type="button" onclick="action_form_pou()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
