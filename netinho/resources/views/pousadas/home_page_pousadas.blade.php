@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_guest')
@endsection

@section('content')
  <section id="pousadas_cards">
    @include('pousadas.guest_card_pousada')
  </section>
@endsection

@section('javascript')
<script type="text/javascript">
    var img_viewer_control = 0;
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page("pousada", 'pousadas_cards');
    });
</script>
@endsection

