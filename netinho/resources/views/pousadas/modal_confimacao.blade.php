<div class = "modal fade" id = "confirm_pou_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-md" role = "document">
        <div class = "modal-content">
            <div class="modal-header bg-info">
                <h4 class="text-white" id="confirm_pou_title"></h4>
        
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <div class = "modal-body">
                <div class="row">
                    <div class="col-12 mb-5" id="confirm_pou_msg">
                    </div>
                    <input id="confirm_pou_id" class="d-none">
                </div>
                <div id="snackbar3">Mensagem de erro:</div>
            </div>

            <div class="modal-footer">
                <div class="form-group col-12 text-right">
                    <button class="btn btn-primary mr-1" id='confirm_pou_button' type="button"
                        onclick="action_confirm_pou()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
