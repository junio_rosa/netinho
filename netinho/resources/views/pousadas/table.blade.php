<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$pousadas->count()}} pousadas de {{$pousadas->total()}} ({{$pousadas->firstItem()}} a {{$pousadas->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $pousadas->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Pousada</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($pousadas as $pou)
                <tr class="text-center">
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='{{$pou->imagem_destaque}}'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong>{{$pou->nome}}<br>

                        <strong>
                            Endereço:
                        </strong>{{$pou->endereco}}<br>

                        <strong>
                            Tamanho:
                        </strong>{{$pou->tamanho}}<br>

                        <strong>
                            Diária:
                        </strong>{{$pou->diaria}}<br>

                        <strong>
                            Telefone:
                        </strong>{{$pou->telefone}}<br>

                        <strong>
                            Possui piscina:
                        </strong>@if ($pou->piscina)
                            Sim
                            @else
                            Não
                        @endif<br>
                    </td>
                    <td class="align-middle">{{$pou->id}}</td>
                    <td class="align-middle">
                        @isset($able)
                            @if ($able)
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_pou({{$pou}}, {{$pou->galeria}})">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_pou({{$pou}}, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            @else
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_pou({{$pou}}, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            @endif
                        @endisset
                    </td>
                    @php
                        $i++;
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$pousadas->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>
