<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel = "icon" href = "{{ asset('assets/icons/favicon.ico') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
        <link href="{{ asset('css/snackbars.css') }}" rel="stylesheet">
        <title>Resolve Escarpas</title>
    </head>

    <body class="total_vh">
        <div class="row h-100">
            <div class="col-sm-12 my-auto text-center">
                <img src = "{{ asset('assets/icons/404.png') }}">
                <div class="col-12">
                    <h2>404</h2>
                </div>
                <div class="col-12">
                    <h3>Página não encontrada</h3>
                </div>
                <div class="col-12 font-weight-bold">
                    A página que você está procurando não existe ou um outro erro ocurreu.<br>
                    <a href="/">Clique aqui</a> para ser redirecionado para a página principal.
                </div>
            </div>
         </div>
    </body>
</html>
