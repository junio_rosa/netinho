@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_auth')
@endsection

@section('content')
    @include('layouts.home_page')
@endsection

@section('javascript')
<script type="text/javascript">
    var img_viewer_control;
    var datas_bd = new Array();
    var datas_para_deletar = new Array();
    var datas_para_upload = new Array();
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page();
    });
</script>
@endsection

