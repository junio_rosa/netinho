@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_auth')
@endsection

@section('content')
<div class="container">
    <div class="card border my-4">
        <div class="card-header">
            <div class="container">
                <h4 class="card-title text-secondary">
                    Painel Administrativo de Passeios de Lancha
                </h4>
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="input-group" id="lancha_search_group">
                            <input type="text" class="form-control"
                                    id="lancha_search" placeholder="Busque por passeios de lancha..."
                                    autocomplete="on">
                            <div class="input-group-append" id="lancha_search_button">
                                <button class="btn btn-secondary rounded-right" type="button"
                                    onclick="fetch_data_passeios_lancha()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>

                        <div id="lancha_autocomplete" class="dropdown"></div>
                    </div>

                    <div class="form-group col-12">
                        <label>Selecione o tipo de busca:</label>
                        <select class='form-control w-100' id='lancha_select' onchange="select_on_change('lancha_search', 'lancha_autocomplete')">
                            <option value='nome' selected='selected'>Buscar por Nome</option>
                            <option value='nome_del'>Buscar por Nome (Passeios lancha desabilitadas)</option>
                        </select>
                    </div>

                    <div class="form-group ml-auto">
                        <button class="btn btn-primary text-white" onclick="open_form_lancha(0,'','')">
                            <i class="fas fa-plus-circle"></i> Novo Passeio Lancha
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <section id="tabela_passeios_lancha">
            @include('passeio_lancha.table')
        </section>

        <input id="paginacao_lancha" value="1" class="d-none">
    </div>
</div>

<section>
    @include('passeio_lancha.modal_form')
</section>

<section>
    @include('passeio_lancha.modal_confirmacao')
</section>
@endsection

@section('javascript')
<script type="text/javascript">

    var galeriaFileArray = new Array();
    var datas_para_upload = new Array();
    var datas_para_deletar = new Array();
    var datas_bd = new Array();
    var img_viewer_control = 0;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    $(function(){
        $('.editor').jqte();
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        document.getElementById('paginacao_lancha').value = pagina_atual;
        fetch_data_passeios_lancha();
    });
</script>
@endsection
