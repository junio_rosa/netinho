<div class="container">
  <div class="form-row">
      <div class="col-12">
          <h1>
              {{$search_result[0]->nome}}
          </h1>
      </div>

      <div class="col-12 my-3">
          @php
              echo($search_result[0]->horarios);
          @endphp
      </div>

      <div class="col-12 my-3">
          @php
              echo($search_result[0]->descricao);
          @endphp
      </div>

      <div class="col-12">
          @php
              $home_page = true;
          @endphp
          @include('helpers.galeria', compact('home_page'))
      </div>

      <div class="col-12 col-md-8 my-5">
          <label class="font-weight-bold">Calendário:</label>
          @include('helpers.calendario')
      </div>

      <div id="snackbar2"></div>
  </div>
</div>
