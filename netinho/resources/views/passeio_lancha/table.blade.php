<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$passeio_lancha->count()}} passeios lancha de {{$passeio_lancha->total()}} ({{$passeio_lancha->firstItem()}} a {{$passeio_lancha->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $passeio_lancha->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Passeio lancha</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($passeio_lancha as $lancha)
                <tr class="text-center">
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='{{$lancha->imagem_destaque}}'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong>{{$lancha->nome}}<br>
                    </td>
                    <td class="align-middle">{{$lancha->id}}</td>
                    <td class="align-middle">
                        @isset($able)
                            @if ($able)
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_lancha({{$lancha}}, {{$lancha->galeria}}, {{$lancha->calendario}})">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_lancha({{$lancha}}, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            @else
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_lancha({{$lancha}}, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            @endif
                        @endisset
                    </td>
                    @php
                        $i++;
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$passeio_lancha->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>
