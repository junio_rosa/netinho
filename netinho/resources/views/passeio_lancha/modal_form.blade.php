<div class = "modal fade" id = "form_lancha_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-xl" role = "document">
        <div class = "modal-content">

            <div class="modal-header bg-info">
                <h4 class="text-white" id="form_lancha_title"></h4>
        
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    {{-- ID --}}
                    <input id="form_lancha_id" class="d-none">
                    {{-- NOME --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Nome:</label>
                        <input class="form-control w-100" type="text" id="form_lancha_nome" autocomplete="on"
                                onfocusout="validacao_campo('form_lancha_nome', 'small_lancha_nome', 'Campo Validado', 'Nome inválido', 1)">
                        <small id="small_lancha_nome" class="text-secondary">
                            Digite o nome ou motorista do passeio
                        </small>
                    </div>
                    {{-- HORARIOS --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Horários:</label>
                        <textarea class="form-control editor" type="text" id="form_lancha_horarios"></textarea>
                    </div>
                    {{-- DESCRIÇÃO --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Descrição:</label>
                        <textarea class="form-control editor" type="text" id="form_lancha_descricao"></textarea>
                    </div>
                    {{-- CALENDÁRIO DE LOCAÇÃO --}}
                    <label class="font-weight-bold">Calendário de locação:</label>
                    @include('helpers.calendario')
                    {{-- IMAGEM DESTAQUE --}}
                    <div class='form-group col-12'>
                        @include('helpers.img_destaque')
                    </div>
                    {{-- GALERIA --}}
                    <div class="form-group col-12">
                        @include('helpers.galeria')
                    </div>
                </div>

                <div id="snackbar2">Mensagem de erro:</div>
            </div>

            <div class="modal-footer">
                <div class="form-group col-12 text-right">
                    {{-- BUTTON REG UPG --}}
                    <button class="btn btn-primary mr-1" id="form_lancha_button" type="button" onclick="action_form_lancha()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
