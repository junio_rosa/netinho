@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_guest')
@endsection

@section('content')
  <section id="casas_cards">
    @include('casas.guest_card_casa')
  </section>
@endsection

@section('javascript')
<script type="text/javascript">
    var img_viewer_control = 0;
    var datas_para_upload = new Array();
    var datas_para_deletar = new Array();
    var datas_bd = new Array();
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page("casa", 'casas_cards');
    });
</script>
@endsection

