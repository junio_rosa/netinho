@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_auth')
@endsection

@section('content')
<div class="container">
    <div class="card border my-4">
        <div class="card-header">
            <div class="container">
                <h4 class="card-title text-secondary">
                    Painel Administrativo de Casas
                </h4>
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="input-group" id="cas_search_group">
                            <input type="text" class="form-control"
                                    id="cas_search" placeholder="Busque por casas..."
                                    autocomplete="on">
                            <div class="input-group-append" id="cas_search_button">
                                <button class="btn btn-secondary rounded-right" type="button"
                                    onclick="fetch_data_casas()">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>

                        <div id="cas_autocomplete" class="dropdown"></div>
                    </div>

                    <div class="form-group col-12">
                        <label>Selecione o tipo de busca:</label>
                        <select class='form-control w-100' id='cas_select' onchange="select_on_change('cas_search', 'cas_autocomplete')">
                            <option value='nome' selected='selected'>Buscar por Nome</option>
                            <option value='endereco'>Buscar por Endereço</option>
                            <option value='nome_del'>Buscar por Nome (Casas desabilitadas)</option>
                            <option value='endereco_del'>Buscar por Endereço (Casas desabilitadas)</option>
                        </select>
                    </div>

                    <div class="form-group ml-auto">
                        <button class="btn btn-primary text-white" onclick="open_form_cas(0, '', '')">
                            <i class="fas fa-plus-circle"></i> Nova Casa
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <section id="tabela_casas">
            @include('casas.table')
        </section>

        <input id="paginacao_casas" value="1" class="d-none">
    </div>
</div>

<section>
    @include('casas.modal_form')
</section>

<section>
    @include('casas.modal_confimacao')
</section>
@endsection

@section('javascript')
<script type="text/javascript">

    var galeriaFileArray = new Array();
    var datas_para_upload = new Array();
    var datas_para_deletar = new Array();
    var datas_bd = new Array();
    var img_viewer_control = 0;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    $(function(){
        $('.editor').jqte();
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        document.getElementById('paginacao_casas').value = pagina_atual;
        fetch_data_casas();
    });
</script>
@endsection
