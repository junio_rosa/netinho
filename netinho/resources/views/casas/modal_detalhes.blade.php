<div class="modal fade" id="modal_casa_details" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header text-white bg-info">
          <h4 class="heading">Detalhes da casa</h4>
  
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="white-text">&times;</span>
          </button>
        </div>
  
        <div class="modal-body">
          <div class="form-row">
            <div class="col-12 col-lg-5">
              <a class="galeria-preview-thumb" id="details_cas_img_link" rel="galeria-preview-thumb" href="" title="Imagem destaque">
                <img class="img-fluid" id="details_cas_img">
              </a>
            </div>
  
            <div class="col-12 col-lg-7">
                <h3>
                  <p id="details_cas_nome"></p>
                </h3>
                <p id="details_cas_endereco"><p>  
                <p id="details_cas_max_ocupantes"></p>
                <p id="details_cas_piscina"></p>
                <p id="details_cas_sauna"></p>
                <p id="details_cas_churrasqueira"></p>
            </div>

            
            <label class="font-weight-bold">Calendário:</label>
            @include('helpers.calendario')
            

            <div class="col-12 w-100 my-3">
                <label class="font-weight-bold">Descrição:</label>
                <div class="w-100" id="details_cas_descricao"></div>
            </div>

            @php
                $home_page = true
            @endphp
            @include('helpers.galeria', compact('home_page'))
            <div id="snackbar2"></div>
          </div>
        </div>

        <div class="modal-footer">
          <button class="btn btn-danger" data-dismiss = "modal">
            <i class="fas fa-times-circle text-white"></i> Fechar
          </button>
        </div>
      </div>
    </div>
</div>
