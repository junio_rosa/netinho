<div class = "modal fade" id = "form_cas_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-xl" role = "document">
        <div class = "modal-content">

            <div class="modal-header bg-info">
                <h4 class="text-white" id="form_cas_title"></h4>
        
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    {{-- ID --}}
                    <input id="form_cas_id" class="d-none">
                    {{-- NOME --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Nome:</label>
                        <input class="form-control w-100" type="text" id="form_cas_nome" autocomplete="on"
                                onfocusout="validacao_campo('form_cas_nome', 'small_cas_nome', 'Campo Validado', 'Nome inválido', 1)">
                        <small id="small_cas_nome" class="text-secondary">
                            Digite o nome da casa
                        </small>
                    </div>
                    {{-- ENDEREÇO --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Endereço:</label>
                        <input class="form-control w-100" type="text" id="form_cas_endereco" autocomplete="on"
                                onfocusout="validacao_campo('form_cas_endereco', 'small_cas_endereco', 'Campo Validado', 'Endereço inválido', 1)">
                        <small id="small_cas_endereco" class="text-secondary">
                            Digite o endereço da casa
                        </small>
                    </div>
                    {{-- OCUPANTES --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Nº máximo de ocupantes:</label>
                        <input class="form-control w-100"
                                type="input"
                                id="form_cas_ocupantes" autocomplete="on"
                                onfocusout="validacao_campo('form_cas_ocupantes', 'small_cas_ocupantes', 'Campo Validado', 'Campo ocupantes inválido', 1)">
                        <small id="small_cas_ocupantes" class="text-secondary">
                            Digite o número máximo de ocupantes
                        </small>
                    </div>
                    {{-- CHURRASQUEIRA --}}
                    <div class="form-group col-12 col-lg-4 ml-4 text-left ml-lg-0 text-lg-center">
                        <input type="checkbox" class="form-check-input" id="form_cas_churrasqueira">
                        <label class="font-weight-bold" for="form_cas_churrasqueira">Possui churrasqueira</label>
                    </div>
                    {{-- PISCINA --}}
                    <div class="form-group col-12 col-lg-4 ml-4 text-left ml-lg-0 text-lg-center">
                        <input type="checkbox" class="form-check-input" id="form_cas_piscina">
                        <label class="font-weight-bold" for="form_cas_piscina">Possui piscina</label>
                    </div>
                    {{-- SAUNA --}}
                    <div class="form-group col-12 col-lg-4 ml-4 text-left ml-lg-0 text-lg-center">
                        <input type="checkbox" class="form-check-input" id="form_cas_sauna">
                        <label class="font-weight-bold" for="form_cas_sauna">Possui sauna</label>
                    </div>
                    {{-- DESCRIÇÃO --}}
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Descrição:</label>
                        <textarea class="form-control editor" type="text" id="form_cas_descricao"></textarea>
                    </div>
                    {{-- CALENDÁRIO DE LOCAÇÃO --}}
                    <label class="font-weight-bold">Calendário:</label>
                    @include('helpers.calendario')
                    {{-- IMAGEM DESTAQUE --}}
                    <div class='form-group col-12'>
                        @include('helpers.img_destaque')
                    </div>
                    {{-- GALERIA --}}
                    <div class="form-group col-12">
                        @include('helpers.galeria')
                    </div>
                </div>

                <div id="snackbar2">Mensagem de erro:</div>
            </div>

            <div class="modal-footer">
                <div class="form-group col-12 text-right">
                    {{-- BUTTON REG UPG --}}
                    <button class="btn btn-primary mr-1" id="form_cas_button" type="button" onclick="action_form_cas()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
