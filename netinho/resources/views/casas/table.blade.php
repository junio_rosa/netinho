<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo {{$casas->count()}} casas de {{$casas->total()}} ({{$casas->firstItem()}} a {{$casas->lastItem()}})
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                @php
                    $i = $casas->firstItem();
                @endphp
                <tr class="text-center">
                    <th>#</th>
                    <th>Casas</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($casas as $cas)
                <tr class="text-center">
                    <td class="align-middle">{{$i}}</td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='{{$cas->imagem_destaque}}'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong>{{$cas->nome}}<br>

                        <strong>
                            Endereço:
                        </strong>{{$cas->endereco}}<br>

                        <strong>
                            Nº máximo de Ocupantes:
                        </strong>{{$cas->max_ocupantes}}<br>

                        <strong>
                            Possui sauna:
                        </strong>@if ($cas->sauna)
                            Sim
                            @else
                            Não
                        @endif<br>

                        <strong>
                            Possui churrasqueira:
                        </strong>@if ($cas->churrasqueira)
                            Sim
                            @else
                            Não
                        @endif<br>

                        <strong>
                            Possui piscina:
                        </strong>@if ($cas->piscina)
                            Sim
                            @else
                            Não
                        @endif<br>
                    </td>
                    <td class="align-middle">{{$cas->id}}</td>
                    <td class="align-middle">
                        @isset($able)
                            @if ($able)
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_cas({{$cas}}, {{$cas->galeria}}, {{$cas->calendario}})">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_cas({{$cas}}, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            @else
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_cas({{$cas}}, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            @endif
                        @endisset
                    </td>
                    @php
                        $i++;
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                {{$casas->links("pagination::bootstrap-4")}}
            </tfoot>
        </table>
    </div>
</div>
