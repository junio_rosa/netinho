{{-- TABELA CONTROLE DE ARQUIVOS --}}
<div class="table-responsive my-3 d-none">
    <table class="table table-ordered table-hover" id="table_galeria">
        {{-- THEAD COLOCADA POR JAVASCRIPT --}}
        <tbody>
            <thead>
                <tr class="text-center">
                    <th>Código</th>
                    <th>Nome Arquivo</th>
                    <th>Extensão</th>
                    <th>Uploaded</th>
                    <th>ID BD</th>
                    <th>Pos Array File</th>
                    <th>Deletado</th>
                </tr>
            </thead>
        </tbody>
    </table>
</div>


{{-- DIV UPLOAD --}}
<div class="col-12">
    <label class="font-weight-bold">Galeria:</label>
    @if (!isset($home_page))
    <div class="custom-file">
        <input type="file" class="custom-file-input"  name="inputGaleria[]" id="inputGaleria" multiple>
        <label class="custom-file-label" for="inputGaleria" data-browse="Procurar">
            Clique aqui para fazer o upload
        </label>
    </div>
    @endif
</div>

{{-- PREVIEW GALERIA --}}
<div class="div-galeria col-12 
    @if (!isset($home_page))
        mt-4"
    @else
        "
    @endif
    >
    <div class="card w-100 border-0">
        <div class="card-title text-right border-0">
            <button class="btn btn-success my-2" onclick="visualizar_mais()">
                <i class="fas fa-plus-circle"></i> Visualizar mais
            </button>
            <button class="btn btn-primary my-2" onclick="visualizar_menos()">
                <i class="fas fa-minus-circle"></i> Visualizar menos
            </button>
        </div>
        <hr>
        <div class="card-body max-height-galeria">
            <div class="form-row" id="galeria_divContent">
                
            </div>
        </div>
    </div>
</div>
