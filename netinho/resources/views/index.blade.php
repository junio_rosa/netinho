@extends('layouts.app')

@section('navbar')
    @include('navbars.navbar_guest')
@endsection

@section('content')
    @include('layouts.home_page')
@endsection

@section('javascript')
<script type="text/javascript">
    
</script>
@endsection

