<div class = "navbar navbar-expand-lg bg-light mb-0" id = 'div_navbar'>
    <div class = "container">
        <a class = "navbar-brand" href="/">
            <img id = "logo_navbar" src = "{{asset('assets/icons/logo.png')}}">
        </a>
        <button class = "navbar-toggler bg-dark" type = "button" data-toggle = "collapse" data-target = "#navbar_dropdown">
            <i class="fas fa-bars text-white"></i>
        </button>

        <div class = "form-inline collapse navbar-collapse" id = "navbar_dropdown">
            <ul class = "navbar-nav ml-auto">
                <li>
                    <a class = "btn btn-outline-navbar my-2 my-lg-0 mr-2 font-weight-bold"
                    href = "/home/pousadas">
                        Pousadas
                    </a>
                </li>
                <li>
                    <a class = "btn btn-outline-navbar my-2 my-lg-0 mr-2 font-weight-bold"
                    href = "/home/casas">
                        Casas
                    </a>
                </li>
                <li>
                    <a class = "btn btn-outline-navbar my-2 my-lg-0 mr-2 font-weight-bold"
                    href = "/home/passeio4x4">
                        Passeio 4x4
                    </a>
                </li>
                <li>
                    <a class = "btn btn-outline-navbar my-2 my-lg-0 mr-2 font-weight-bold"
                    href = "/home/passeio_lancha">
                        Passeio Lancha
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 font-weight-bold"   
                    href = "/sobre_nos">
                        Sobre Nós
                    </a>
                </li>
                <li>
                    <a class="nav-link mr-2 font-weight-bold" href="https://www.facebook.com/resolveescarpas" target="_blank">
                        <i class="fab fa-facebook-square"></i> Facebook
                    </a>
                </li>
                <li>
                    <a class="nav-link mr-2 font-weight-bold" href="https://www.instagram.com/resolveescarpas/" target="_blank">
                        <i class="fab fa-instagram"></i> Instagram
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<hr class="my-0">