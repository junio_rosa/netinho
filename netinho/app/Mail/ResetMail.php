<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class ResetMail extends Mailable
{
    public $user;
    public $token;

    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }


    public function build()
    {
        return $this->from('suporteresolveescarpas@gmail.com')
                    ->to($this->user->email)
                    ->subject('Redefinir senha no site Resolve Escarpas')
                    ->markdown('emails.reset_mail')
                    ->with([
                        'user' => $this->user,
                        'token' => $this->token
                    ]);
    }
}
