<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pousada extends Model
{
    use HasFactory;
    use SoftDeletes;

    function scopePousadaTrash($query, $able){
        if(!$able){
            return $query->onlyTrashed();
        }
    }

    function scopeComparePousadas($query, $input, $select){
        return $query->where($select, 'like', '%'.$input.'%')
                    ->orderBy($select, 'asc');
    }

    function scopePousadaSection($query, $autocomplete){
        if($autocomplete){
            return $query->limit(10)->get();
        }else{
            return $query->paginate(10);
        }
    }

    function galeria(){
        return $this->hasMany('App\Models\GaleriaPousada', 'id_pousada', 'id')
            ->select('galeria_pousadas.*');
    }
}
