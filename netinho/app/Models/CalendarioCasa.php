<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalendarioCasa extends Model
{
    use HasFactory;
    protected $casts = [
        'data_aluguel' => 'date:Y-m-d',
    ];
}
