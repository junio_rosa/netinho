<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Casa extends Model
{
    use HasFactory;
    use SoftDeletes;

    function scopeCasaTrash($query, $able){
        if(!$able){
            return $query->onlyTrashed();
        }
    }

    function scopeCompareCasas($query, $input, $select){
        return $query->where($select, 'like', '%'.$input.'%')
                    ->orderBy($select, 'asc');
    }

    function scopeHomePage($query, $field_1, $field_2){
        return $query->join('calendario_casas', 'casas.id', '=', 'calendario_casas.id_casa')
                     ->whereBetween('data_aluguel', [$field_1, $field_2])
                     ->orderBy('nome', 'asc')
                     ->select('casas.*')
                     ->get();
    }

    function scopeNotIn($query, $field){
        return $query->whereNotIn('id', $field)
                     ->orderBy('nome', 'asc')
                     ->paginate('10');
    }

    function scopeCasaSection($query, $autocomplete){
        if($autocomplete){
            return $query->limit(10)->get();
        }else{
            return $query->paginate(10);
        }
    }

    function galeria(){
        return $this->hasMany('App\Models\GaleriaCasa', 'id_casa', 'id')
            ->select('galeria_casas.*');
    }

    function calendario(){
        $year = date("Y");
        return $this->hasMany('App\Models\CalendarioCasa', 'id_casa', 'id')
            ->whereDate('data_aluguel', '>=', $year.'-01-01')
            ->select('calendario_casas.data_aluguel');
    }
}
