<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaCasa extends Model
{
    use HasFactory;

    function casa(){
        return $this->belongsTo('App\Models\Casa', 'id_casa', 'id')
            ->select('casas.*');
    }
}
