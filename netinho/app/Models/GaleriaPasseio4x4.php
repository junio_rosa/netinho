<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaPasseio4x4 extends Model
{
    use HasFactory;

    protected $table = 'galeria_passeios4x4';

    function passeio4x4(){
        return $this->belongsTo('App\Models\Passeio4x4', 'id_passeio4x4', 'id')
            ->select('passeios4x4.*');
    }
}
