<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Passeio4x4 extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'passeios4x4';

    function scopePasseio4x4Trash($query, $able){
        if(!$able){
            return $query->onlyTrashed();
        }
    }

    function scopeComparePasseio4x4($query, $input, $select){
        return $query->where($select, 'like', '%'.$input.'%')
                    ->orderBy($select, 'asc');
    }

    function scopePasseio4x4Section($query, $autocomplete){
        if($autocomplete){
            return $query->limit(10)->get();
        }else{
            return $query->paginate(10);
        }
    }

    function scopeHomePage($query, $field_1, $field_2){
        return $query->join('calendario_passeio4x4', 'passeios4x4.id', '=', 'calendario_passeio4x4.id_passeio4x4')
                     ->whereBetween('data_aluguel', [$field_1, $field_2])
                     ->orderBy('nome', 'asc')
                     ->select('passeios4x4.*')
                     ->get();
    }

    function scopeNotIn($query, $field){
        return $query->whereNotIn('id', $field)
                     ->orderBy('nome', 'asc')
                     ->paginate('10');
    }

    function galeria(){
        return $this->hasMany('App\Models\GaleriaPasseio4x4', 'id_passeio4x4', 'id')
            ->select('galeria_passeios4x4.*');
    }

    function calendario(){
        $year = date("Y");
        return $this->hasMany('App\Models\CalendarioPasseio4x4', 'id_passeio4x4', 'id')
            ->whereDate('data_aluguel', '>=', $year.'-01-01')
            ->select('calendario_passeio4x4.data_aluguel');
    }
}
