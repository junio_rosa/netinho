<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaPousada extends Model
{
    use HasFactory;

    function pousada(){
        return $this->belongsTo('App\Models\Pousada', 'id_pousada', 'id')
            ->select('pousadas.*');
    }
}
