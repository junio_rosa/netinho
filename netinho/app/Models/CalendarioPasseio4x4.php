<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalendarioPasseio4x4 extends Model
{
    use HasFactory;

    protected $table = 'calendario_passeio4x4';
}
