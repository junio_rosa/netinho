<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaPasseioLancha extends Model
{
    use HasFactory;

    protected $table = 'galeria_passeios_lancha';

    function passeio_lancha(){
        return $this->belongsTo('App\Models\PasseioLancha', 'id_passeio_lancha', 'id')
            ->select('passeios_lancha.*');
    }

}
