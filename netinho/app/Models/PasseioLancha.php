<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasseioLancha extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'passeios_lancha';

    function scopePasseioLanchaTrash($query, $able){
        if(!$able){
            return $query->onlyTrashed();
        }
    }

    function scopeComparePasseioLancha($query, $input, $select){
        return $query->where($select, 'like', '%'.$input.'%')
                    ->orderBy($select, 'asc');
    }

    function scopePasseioLanchaSection($query, $autocomplete){
        if($autocomplete){
            return $query->limit(10)->get();
        }else{
            return $query->paginate(10);
        }
    }

    function scopeHomePage($query, $field_1, $field_2){
        return $query->join('calendario_passeio_lancha', 'passeios_lancha.id', '=', 'calendario_passeio_lancha.id_passeio_lancha')
                     ->whereBetween('data_aluguel', [$field_1, $field_2])
                     ->orderBy('nome', 'asc')
                     ->select('passeios_lancha.*')
                     ->get();
    }

    function scopeNotIn($query, $field){
        return $query->whereNotIn('id', $field)
                     ->orderBy('nome', 'asc')
                     ->paginate('10');
    }

    function galeria(){
        return $this->hasMany('App\Models\GaleriaPasseioLancha', 'id_passeio_lancha', 'id')
            ->select('galeria_passeios_lancha.*');
    }

    function calendario(){
        $year = date("Y");
        return $this->hasMany('App\Models\CalendarioPasseioLancha', 'id_passeio_lancha', 'id')
            ->whereDate('data_aluguel', '>=', $year.'-01-01')
            ->select('calendario_passeio_lancha.data_aluguel');
    }
}
