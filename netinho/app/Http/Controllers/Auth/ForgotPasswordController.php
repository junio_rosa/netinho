<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ResetMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function sendResetLinkEmail(Request $request)
    {
        $count_registered = User::where('email', $request->email)->get()->count();
        if($count_registered > 0){
            $count = DB::table('password_resets')->where('email', $request->email)->get()->count();
            $token = Str::random(60);
            if($count > 0){
                DB::table('password_resets')
                    ->where('email', $request->email)
                    ->update(['token' => Hash::make($token)]);
            }else{
                DB::table('password_resets')->insert([
                    'email' => $request->email,
                    'token' => Hash::make($token),
                    'created_at' => Carbon::now()
                ]);
            }
            if ($this->sendResetEmail($request->email, $token)) {
                $msg = array();
                array_push($msg, 'Um link para redefinir sua senha foi enviado para o seu endereço de email.');
                $type = 'alert-success';
                return view('auth.passwords.email', compact('msg', 'type'));
            } else {
                $msg = array();
                array_push($msg, 'ERRO:');
                array_push($msg, '');
                array_push($msg, '');
                array_push($msg, 'Ocorreu um erro de internet. Por favor tente mais tarde.');
                $type = 'alert-danger';
                return view('auth.passwords.email', compact('msg', 'type'));
            }
        }else{
            $msg = array();
            array_push($msg, 'ERRO:');
            array_push($msg, '');
            array_push($msg, '');
            array_push($msg, 'Usuário não existe!!');
            $type = 'alert-danger';
            return view('auth.passwords.email', compact('msg', 'type'));
        }
    }

    private function sendResetEmail($email, $token){
        try {
            $user = User::where('email', $email)->select('name', 'email')->first();
            Mail::to($user->email)
                ->send(new ResetMail($user, $token));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
