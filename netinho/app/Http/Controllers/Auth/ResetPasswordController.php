<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    public function reset(Request $request)
    {
        $token = $request->input('token');
        $email = $request->input('email');
        $senha = $request->input('password');
        $count = DB::table('password_resets')
            ->where('email', $email)->get();
        if(count($count) > 0){
            if(Hash::check($token, $count[0]->token)){
                $credentials = [
                    'email' => $email,
                    'password' => $senha
                ];
                $user = User::where('email', $request->email)->get();
                $user[0]->password = Hash::make($senha);
                $user[0]->save();
                $authOK = Auth::guard()->attempt($credentials);
                if($authOK){
                    return redirect()->intended(route('home'));
                }
            }else{
                $msg = array();
                array_push($msg, 'ERRO:');
                array_push($msg, '');
                array_push($msg, '');
                array_push($msg, 'O email não corresponde com a requisição de redefinir senha,');
                array_push($msg, 'tente novamente enviando uma nova requisição.');
                array_push($msg, 'Caso esse problema volte a acontecer contate o nosso time pelo número:');
                array_push($msg, '(35) 99123-0143');
                $type = 'alert-danger';
                return view('auth.passwords.email', compact('msg', 'type'));
            }
        }else{
            $msg = array();
            array_push($msg, 'ERRO:');
            array_push($msg, '');
            array_push($msg, '');
            array_push($msg, 'Erro de internet, tente novamente redifinir');
            array_push($msg, 'sua senha com o mesmo link enviado por e-mail.');
            array_push($msg, 'Caso esse problema volte a acontecer contate o nosso time pelo número:');
            array_push($msg, '(35) 99123-0143');
            $type = 'alert-danger';
            return view('auth.passwords.email', compact('msg', 'type'));
        }
    }
}
