<?php

namespace App\Http\Controllers;

use App\Models\CalendarioPasseioLancha;
use App\Models\GaleriaPasseioLancha;
use App\Models\Lancha;
use App\Models\PasseioLancha;
use Illuminate\Http\Request;

class PasseioLanchaController extends Controller
{
    public function index(){
        $able = true;
        $passeio_lancha = PasseioLancha::orderBy('nome', 'asc')->paginate(10);
        return view('passeio_lancha.home', compact('passeio_lancha', 'able'));
    }

    public function store(Request $request)
    {
        $msg = '';
        $existe_passeio = PasseioLancha::where('nome', $request->input('nome'))->get()->count();
        if($existe_passeio > 0){
            $msg .= 'Passeio lancha já cadastrado<br>';
        }
        if($msg != ''){
            return $msg;
        }else{
            $ultima_passeio = PasseioLancha::orderBy('id', 'desc')->first();
            $id = 0;
            if($ultima_passeio != NULL){
                $id = $ultima_passeio->id + 1;
            }else{
                $id = 1;
            }
            $passeio = new PasseioLancha();
            $passeio->id = $id;
            $passeio->nome = $request->input('nome');
            $passeio->horarios = $request->input('horarios');
            $passeio->descricao = $request->input('descricao');
            if($request->input('imagem_destaque') != NULL){
                $passeio->imagem_destaque = 'assets/icons/sem_img.png';
            }else{
                $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                $request->file('imagem_destaque')->storeAs('assets/passeio_lancha/'.$id.'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                $passeio->imagem_destaque = 'assets/passeio_lancha/'.$id.'/imagem_destaque/'.$chave_nome_arquivo;
            }
            $passeio->save();
            $tamanho_galeria = $request->input('tamanho_galeria');
            $i = 0;
            while ($i < $tamanho_galeria) {
                $galeria_passeio_lancha = new GaleriaPasseioLancha();
                $galeria_passeio_lancha->id_passeio_lancha = $id;
                $controle_loop = true;
                while($controle_loop){
                    $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                    $existe_foto_galeria = GaleriaPasseioLancha::where('id_passeio_lancha', $id)->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                    if($existe_foto_galeria == 0){
                        $controle_loop = false;
                        $request->file('galeria_foto_'.$i)->storeAs('assets/passeio_lancha/'.$id.'/galeria', $chave_nome_arquivo, 'uploads');
                        $galeria_passeio_lancha->url_imagem = 'assets/passeio_lancha/'.$id.'/galeria/'.$chave_nome_arquivo;
                        $galeria_passeio_lancha->save();
                    }
                }
                $i++;
            }
            if(!empty($request->input('datas_para_upload'))){
                $datas_calendario = explode(',', $request->input('datas_para_upload'));
                $i = 0;
                while($i < count($datas_calendario)){
                    $calendario_passeio_lancha = new CalendarioPasseioLancha();
                    $calendario_passeio_lancha->id_passeio_lancha = $id;
                    $calendario_passeio_lancha->data_aluguel = $datas_calendario[$i];
                    $calendario_passeio_lancha->save();
                    $i++;
                }
            }
            $msg = 'Passeio lancha cadastrado com sucesso';
            return $msg;
        }
    }

    public function show(Request $request)
    {
        $input = $request->get('input');
        $select = $request->get('select');
        $autocomplete = 0;
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $select = str_replace('_del', '', $select);
        $passeio_lancha = PasseioLancha::query()->passeioLanchaTrash($able)->comparePasseioLancha($input, $select)->passeioLanchaSection($autocomplete);
        return view('passeio_lancha.table', compact('passeio_lancha', 'able'))->render();
    }

    public function autocomplete(Request $request)
    {
        $input = $request->input('input');
        $select = $request->input('select');
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $autocomplete = 1;
        $select = str_replace('_del', '', $select);
        $passeio_lancha = PasseioLancha::query()->passeioLanchaTrash($able)->comparePasseioLancha($input, $select)->passeioLanchaSection($autocomplete);
        return $passeio_lancha;
    }

    public function destroy($id)
    {
        $passeio = PasseioLancha::find($id);
        if(isset($passeio)){
            $passeio->delete();
            return 'Passeio lancha desabilitado com sucesso';
        }else{
            return 'ERRO:<br><br>Passeio lancha não encontrado';
        }
    }

    public function create($id)
    {
        $passeio = PasseioLancha::onlyTrashed()->find($id);
        if(isset($passeio)){
            $passeio->restore();
            return 'Passeio lancha habilitado com sucesso';
        }else{
            return 'ERRO:<br><br>Passeio lancha não encontrado';
        }
    }

    public function update(Request $request)
    {
        $passeio_lancha = PasseioLancha::find($request->input('id'));
        $msg = '';
        $novas_fotos = array();
        if(isset($passeio_lancha)){
            if($msg == ''){
                $passeio_lancha->nome = $request->input('nome');
                $passeio_lancha->horarios = $request->input('horarios');
                $passeio_lancha->descricao = $request->input('descricao');
                if($request->input('imagem_destaque') == NULL){
                    if($passeio_lancha->imagem_destaque != 'assets/icons/sem_img.png'){
                        unlink($passeio_lancha->imagem_destaque);
                    }
                    $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                    $request->file('imagem_destaque')->storeAs('assets/passeio_lancha/'.$request->input('id').'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                    $passeio_lancha->imagem_destaque = 'assets/passeio_lancha/'.$request->input('id').'/imagem_destaque/'.$chave_nome_arquivo;
                }
                $passeio_lancha->save();
                $tamanho_galeria = $request->input('tamanho_galeria');
                $i = 0;
                while ($i < $tamanho_galeria) {
                    $galeria_lancha = new GaleriaPasseioLancha();
                    $galeria_lancha->id_passeio_lancha = $request->input('id');
                    $controle_loop = true;
                    while($controle_loop){
                        $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                        $count_registered = GaleriaPasseioLancha::where('id_passeio_lancha', $request->input('id'))->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                        if($count_registered == 0){
                            $controle_loop = false;
                            $request->file('galeria_foto_'.$i)->storeAs('assets/passeio_lancha/'.$request->input('id').'/galeria', $chave_nome_arquivo, 'uploads');
                            $galeria_lancha->url_imagem = 'assets/passeio_lancha/'.$request->input('id').'/galeria/'.$chave_nome_arquivo;
                            $galeria_lancha->save();
                            array_push($novas_fotos, $galeria_lancha);
                        }
                    }
                    $i++;
                }
                if($request->input('tamanho_deletar_galeria') > 0){
                    $i = 0;
                    while($i < $request->input('tamanho_deletar_galeria')){
                        $galeria_lancha = GaleriaPasseioLancha::find($request->input('deletar_galeria_foto_'.$i));
                        unlink($galeria_lancha->url_imagem);
                        $galeria_lancha->delete();
                        $i++;
                    }
                }
                if(!empty($request->input('datas_para_deletar'))){
                    $datas_calendario = explode(',', $request->input('datas_para_deletar'));
                    $i = 0;
                    while($i < count($datas_calendario)){
                        $calendario = CalendarioPasseioLancha::where('id_passeio_lancha', $request->input('id'))->where('data_aluguel', $datas_calendario[$i])->get();
                        $calendario[0]->delete();
                        $i++;
                    }
                }

                if(!empty($request->input('datas_para_upload'))){
                    $datas_calendario = explode(',', $request->input('datas_para_upload'));
                    $i = 0;
                    while($i < count($datas_calendario)){
                        $calendario = new CalendarioPasseioLancha();
                        $calendario->id_passeio_lancha = $request->input('id');
                        $calendario->data_aluguel = $datas_calendario[$i];
                        $calendario->save();
                        $i++;
                    }
                }
                $year = date("Y");
                $datas_bd = CalendarioPasseioLancha::where('id_passeio_lancha', $request->input('id'))
                            ->whereDate('data_aluguel', '>=', $year.'-01-01')
                            ->select('data_aluguel')->get();
                $msg = 'Passeio lancha atualizado com sucesso';
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                    'datas_bd' => $datas_bd,
                );
                return $array;
            }else{
                $msg = "ERRO:<br><br>".$msg;
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                );
                return $array;
            }
        }else{
            $msg = 'ERRO:<br><br>Passeio lancha não encontrado';
            $array = array(
                'msg' => $msg,
                'novas_fotos' => $novas_fotos,
            );
            return $array;
        }
    }
}
