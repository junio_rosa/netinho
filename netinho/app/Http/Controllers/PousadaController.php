<?php

namespace App\Http\Controllers;

use App\Models\GaleriaPousada;
use App\Models\Pousada;
use Illuminate\Http\Request;

class PousadaController extends Controller
{
    public function index(){
        $able = true;
        $pousadas = Pousada::orderBy('nome', 'asc')->paginate(10);
        return view('pousadas.home', compact('pousadas', 'able'));
    }

    public function store(Request $request)
    {
        $msg = '';
        $existe_pousada = Pousada::where('nome', $request->input('nome'))->get()->count();
        if($existe_pousada > 0){
            $msg .= 'Pousada já cadastrada<br>';
        }
        if($msg != ''){
            return $msg;
        }else{
            $ultima_pousada = Pousada::orderBy('id', 'desc')->first();
            $id = 0;
            if($ultima_pousada != NULL){
                $id = $ultima_pousada->id + 1;
            }else{
                $id = 1;
            }
            $pousada = new Pousada();
            $pousada->id = $id;
            $pousada->nome = $request->input('nome');
            $pousada->endereco = $request->input('endereco');
            $pousada->tamanho = $request->input('tamanho');
            $pousada->diaria = $request->input('diaria');
            $pousada->telefone = $request->input('telefone');
            $pousada->descricao = $request->input('descricao');
            $pousada->piscina = $request->input('piscina');
            if($request->input('imagem_destaque') != NULL){
                $pousada->imagem_destaque = 'assets/icons/sem_img.png';
            }else{
                $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                $request->file('imagem_destaque')->storeAs('assets/pousadas/'.$id.'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                $pousada->imagem_destaque = 'assets/pousadas/'.$id.'/imagem_destaque/'.$chave_nome_arquivo;
            }
            $pousada->save();
            $tamanho_galeria = $request->input('tamanho_galeria');
            $i = 0;
            while ($i < $tamanho_galeria) {
                $galeria_pousada = new GaleriaPousada();
                $galeria_pousada->id_pousada = $id;
                $controle_loop = true;
                while($controle_loop){
                    $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                    $existe_foto_galeria = GaleriaPousada::where('id_pousada', $id)->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                    if($existe_foto_galeria == 0){
                        $controle_loop = false;
                        $request->file('galeria_foto_'.$i)->storeAs('assets/pousadas/'.$id.'/galeria', $chave_nome_arquivo, 'uploads');
                        $galeria_pousada->url_imagem = 'assets/pousadas/'.$id.'/galeria/'.$chave_nome_arquivo;
                        $galeria_pousada->save();
                    }
                }
                $i++;
            }
            $msg = 'Pousada cadastrada com sucesso';
            return $msg;
        }
    }

    public function show(Request $request)
    {
        $input = $request->get('input');
        $select = $request->get('select');
        $autocomplete = 0;
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $select = str_replace('_del', '', $select);
        $pousadas = Pousada::query()->pousadaTrash($able)->comparePousadas($input, $select)->pousadaSection($autocomplete);
        return view('pousadas.table', compact('pousadas', 'able'))->render();
    }

    public function autocomplete(Request $request)
    {
        $input = $request->input('input');
        $select = $request->input('select');
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $autocomplete = 1;
        $select = str_replace('_del', '', $select);
        $pousadas = Pousada::query()->pousadaTrash($able)->comparePousadas($input, $select)->pousadaSection($autocomplete);
        return $pousadas;
    }

    public function destroy($id)
    {
        $pousada = Pousada::find($id);
        if(isset($pousada)){
            $pousada->delete();
            return 'Pousada desabilitada com sucesso';
        }else{
            return 'ERRO:<br><br>Pousada não encontrada';
        }
    }

    public function create($id)
    {
        $pousada = Pousada::onlyTrashed()->find($id);
        if(isset($pousada)){
            $pousada->restore();
            return 'Pousada habilitada com sucesso';
        }else{
            return 'ERRO:<br><br>Pousada não encontrada';
        }
    }

    public function update(Request $request)
    {
        $pousada = Pousada::find($request->input('id'));
        $msg = '';
        $novas_fotos = array();
        if(isset($pousada)){
            if(strtoupper($request->input('nome')) != strtoupper($pousada->nome)){
                $existe_pousada = Pousada::where('nome', $request->input('nome'))->get()->count();
                if($existe_pousada > 0){
                    $msg .= 'Pousada já cadastrada<br>';
                }
            }
            if($msg == ''){
                $pousada->nome = $request->input('nome');
                $pousada->endereco = $request->input('endereco');
                $pousada->tamanho = $request->input('tamanho');
                $pousada->diaria = $request->input('diaria');
                $pousada->telefone = $request->input('telefone');
                $pousada->piscina = $request->input('piscina');
                $pousada->descricao = $request->input('descricao');
                if($request->input('imagem_destaque') == NULL){
                    if($pousada->imagem_destaque != 'assets/icons/sem_img.png'){
                        unlink($pousada->imagem_destaque);
                    }
                    $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                    $request->file('imagem_destaque')->storeAs('assets/pousadas/'.$request->input('id').'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                    $pousada->imagem_destaque = 'assets/pousadas/'.$request->input('id').'/imagem_destaque/'.$chave_nome_arquivo;
                }
                $pousada->save();
                $tamanho_galeria = $request->input('tamanho_galeria');
                $i = 0;
                while ($i < $tamanho_galeria) {
                    $galeria_pousada = new GaleriaPousada();
                    $galeria_pousada->id_pousada = $request->input('id');
                    $controle_loop = true;
                    while($controle_loop){
                        $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                        $count_registered = GaleriaPousada::where('id_pousada', $request->input('id'))->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                        if($count_registered == 0){
                            $controle_loop = false;
                            $request->file('galeria_foto_'.$i)->storeAs('assets/pousadas/'.$request->input('id').'/galeria', $chave_nome_arquivo, 'uploads');
                            $galeria_pousada->url_imagem = 'assets/pousadas/'.$request->input('id').'/galeria/'.$chave_nome_arquivo;
                            $galeria_pousada->save();
                            array_push($novas_fotos, $galeria_pousada);
                        }
                    }
                    $i++;
                }
                if($request->input('tamanho_deletar_galeria') > 0){
                    $i = 0;
                    while($i < $request->input('tamanho_deletar_galeria')){
                        $galeria_pousada = GaleriaPousada::find($request->input('deletar_galeria_foto_'.$i));
                        unlink($galeria_pousada->url_imagem);
                        $galeria_pousada->delete();
                        $i++;
                    }
                }
                $msg = 'Pousada atualizada com sucesso';
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                );
                return $array;
            }else{
                $msg = "ERRO:<br><br>".$msg;
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                );
                return $array;
            }
        }else{
            $msg = 'ERRO:<br><br>Pousada não encontrada';
            $array = array(
                'msg' => $msg,
                'novas_fotos' => $novas_fotos,
            );
            return $array;
        }
    }
}
