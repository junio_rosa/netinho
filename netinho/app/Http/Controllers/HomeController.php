<?php

namespace App\Http\Controllers;

use App\Models\GaleriaCasa;
use App\Models\GaleriaPasseio4x4;
use App\Models\GaleriaPasseioLancha;
use App\Models\GaleriaPousada;
use App\Models\Pousada;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $array_casa = array();
        $array_pousada = array();
        $array_passeio4x4 = array();
        $array_passeio_lancha = array();
        $array_aux = array();
        $i = 0;
        $qtd = 4;
        $ultima_foto = GaleriaCasa::orderBy('id', 'desc')->first();
        $count_table = GaleriaCasa::count();
        if($count_table < 4){
            $qtd = $count_table;
        }
        while($i < $qtd){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaCasa::where('id', $pic)->select('galeria_casas.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_casa, $galeria[0]->url_imagem);
                $i++;
            }
        }
        $i = 0;
        $qtd = 4;
        $array_aux = array();
        $ultima_foto = GaleriaPousada::orderBy('id', 'desc')->first();
        $count_table = GaleriaPousada::count();
        if($count_table < 4){
            $qtd = $count_table;
        }
        while($i < $qtd){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaPousada::where('id', $pic)->select('galeria_pousadas.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_pousada, $galeria[0]->url_imagem);
                $i++;
            }
        }
        $i = 0;
        $qtd = 8;
        $array_aux = array();
        $ultima_foto = GaleriaPasseio4x4::orderBy('id', 'desc')->first();
        $count_table = GaleriaPasseio4x4::count();
        if($count_table < 8){
            $qtd = $count_table;
        }
        while($i < $count_table){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaPasseio4x4::where('id', $pic)->select('galeria_passeios4x4.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_passeio4x4, $galeria[0]->url_imagem);
                $i++;
            }
        }
        $i = 0;
        $qtd = 8;
        $array_aux = array();
        $ultima_foto = GaleriaPasseioLancha::orderBy('id', 'desc')->first();
        $count_table = GaleriaPasseioLancha::count();
        if($count_table < 8){
            $qtd = $count_table;
        }
        while($i < $count_table){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaPasseioLancha::where('id', $pic)->select('galeria_passeios_lancha.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_passeio_lancha, $galeria[0]->url_imagem);
                $i++;
            }
        }
        return view('home', compact('array_casa', 'array_pousada', 'array_passeio4x4', 'array_passeio_lancha'));
    }

    public function erro_404(){
        return view('errors.404');
    }

    public function atualizar_perfil(Request $request){
        $msg = '';
        $user = User::find($request->input('id'));
        if($user->email != $request->input('email')){
            if($this->verifica_email_db($request->input('email')))  {
                $msg .= 'E-mail já cadastrado<br>';
            }
        }
        if($msg == ''){
            $user->name = $request->input('nome');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('senha'));
            $user->save();
            return 'Perfil atualizado com sucesso';
        }else{
            return 'ERROS:<br><br>'.$msg;
        }
    }

    private function verifica_email_db($email){
        $user = User::where('email', $email)->get()->count();
        if($user > 0){
            return 1;
        }
        return 0;
    }

    public function atualizar_navbar(){
        return view('navbars.navbar_auth')->render();
    }
}
