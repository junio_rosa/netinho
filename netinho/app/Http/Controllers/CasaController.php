<?php

namespace App\Http\Controllers;

use App\Models\CalendarioCasa;
use App\Models\Casa;
use App\Models\GaleriaCasa;
use Illuminate\Http\Request;

class CasaController extends Controller
{
    public function index(){
        $able = true;
        $casas = Casa::orderBy('nome', 'asc')->paginate(10);
        return view('casas.home', compact('casas', 'able'));
    }

    public function store(Request $request)
    {
        $msg = '';
        $existe_casa = Casa::where('nome', $request->input('nome'))->get()->count();
        if($existe_casa > 0){
            $msg .= 'Casa já cadastrada<br>';
        }
        if($msg != ''){
            return $msg;
        }else{
            $ultima_casa = Casa::orderBy('id', 'desc')->first();
            $id = 0;
            if($ultima_casa != NULL){
                $id = $ultima_casa->id + 1;
            }else{
                $id = 1;
            }
            $casa = new Casa();
            $casa->id = $id;
            $casa->nome = $request->input('nome');
            $casa->endereco = $request->input('endereco');
            $casa->max_ocupantes = $request->input('max_ocupantes');
            $casa->descricao = $request->input('descricao');
            $casa->piscina = $request->input('piscina');
            $casa->sauna = $request->input('sauna');
            $casa->churrasqueira = $request->input('churrasqueira');
            if($request->input('imagem_destaque') != NULL){
                $casa->imagem_destaque = 'assets/icons/sem_img.png';
            }else{
                $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                $request->file('imagem_destaque')->storeAs('assets/casas/'.$id.'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                $casa->imagem_destaque = 'assets/casas/'.$id.'/imagem_destaque/'.$chave_nome_arquivo;
            }
            $casa->save();
            $tamanho_galeria = $request->input('tamanho_galeria');
            $i = 0;
            while ($i < $tamanho_galeria) {
                $galeria_casa = new GaleriaCasa();
                $galeria_casa->id_casa = $id;
                $controle_loop = true;
                while($controle_loop){
                    $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                    $existe_foto_galeria = GaleriaCasa::where('id_casa', $id)->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                    if($existe_foto_galeria == 0){
                        $controle_loop = false;
                        $request->file('galeria_foto_'.$i)->storeAs('assets/casas/'.$id.'/galeria', $chave_nome_arquivo, 'uploads');
                        $galeria_casa->url_imagem = 'assets/casas/'.$id.'/galeria/'.$chave_nome_arquivo;
                        $galeria_casa->save();
                    }
                }
                $i++;
            }
            if(!empty($request->input('datas_para_upload'))){
                $datas_calendario = explode(',', $request->input('datas_para_upload'));
                $i = 0;
                while($i < count($datas_calendario)){
                    $calendario_casa = new CalendarioCasa();
                    $calendario_casa->id_casa = $id;
                    $calendario_casa->data_aluguel = $datas_calendario[$i];
                    $calendario_casa->save();
                    $i++;
                }
            }
            $msg = 'Casa cadastrada com sucesso';
            return $msg;
        }
    }

    public function show(Request $request)
    {
        $input = $request->get('input');
        $select = $request->get('select');
        $autocomplete = 0;
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $select = str_replace('_del', '', $select);
        $casas = Casa::query()->casaTrash($able)->compareCasas($input, $select)->casaSection($autocomplete);
        return view('casas.table', compact('casas', 'able'))->render();
    }

    public function autocomplete(Request $request)
    {
        $input = $request->input('input');
        $select = $request->input('select');
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $autocomplete = 1;
        $select = str_replace('_del', '', $select);
        $pousadas = Casa::query()->casaTrash($able)->compareCasas($input, $select)->casaSection($autocomplete);
        return $pousadas;
    }

    public function destroy($id)
    {
        $casa = Casa::find($id);
        if(isset($casa)){
            $casa->delete();
            return 'Casa desabilitada com sucesso';
        }else{
            return 'ERRO:<br><br>Casa não encontrada';
        }
    }

    public function create($id)
    {
        $casa = Casa::onlyTrashed()->find($id);
        if(isset($casa)){
            $casa->restore();
            return 'Casa habilitada com sucesso';
        }else{
            return 'ERRO:<br><br>Casa não encontrada';
        }
    }

    public function update(Request $request)
    {
        $casa = Casa::find($request->input('id'));
        $msg = '';
        $novas_fotos = array();
        if(isset($casa)){
            if(strtoupper($request->input('nome')) != strtoupper($casa->nome)){
                $existe_casa = Casa::where('nome', $request->input('nome'))->get()->count();
                if($existe_casa > 0){
                    $msg .= 'Casa já cadastrada<br>';
                }
            }
            if($msg == ''){
                $casa->nome = $request->input('nome');
                $casa->endereco = $request->input('endereco');
                $casa->max_ocupantes = $request->input('max_ocupantes');
                $casa->descricao = $request->input('descricao');
                $casa->piscina = $request->input('piscina');
                $casa->sauna = $request->input('sauna');
                $casa->churrasqueira = $request->input('churrasqueira');
                if($request->input('imagem_destaque') == NULL){
                    if($casa->imagem_destaque != 'assets/icons/sem_img.png'){
                        unlink($casa->imagem_destaque);
                    }
                    $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                    $request->file('imagem_destaque')->storeAs('assets/casas/'.$request->input('id').'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                    $casa->imagem_destaque = 'assets/casas/'.$request->input('id').'/imagem_destaque/'.$chave_nome_arquivo;
                }
                $casa->save();
                $tamanho_galeria = $request->input('tamanho_galeria');
                $i = 0;
                while ($i < $tamanho_galeria) {
                    $galeria_casa = new GaleriaCasa();
                    $galeria_casa->id_casa = $request->input('id');
                    $controle_loop = true;
                    while($controle_loop){
                        $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                        $count_registered = GaleriaCasa::where('id_casa', $request->input('id'))->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                        if($count_registered == 0){
                            $controle_loop = false;
                            $request->file('galeria_foto_'.$i)->storeAs('assets/casas/'.$request->input('id').'/galeria', $chave_nome_arquivo, 'uploads');
                            $galeria_casa->url_imagem = 'assets/casas/'.$request->input('id').'/galeria/'.$chave_nome_arquivo;
                            $galeria_casa->save();
                            array_push($novas_fotos, $galeria_casa);
                        }
                    }
                    $i++;
                }
                if($request->input('tamanho_deletar_galeria') > 0){
                    $i = 0;
                    while($i < $request->input('tamanho_deletar_galeria')){
                        $galeria_casa = GaleriaCasa::find($request->input('deletar_galeria_foto_'.$i));
                        unlink($galeria_casa->url_imagem);
                        $galeria_casa->delete();
                        $i++;
                    }
                }
                if(!empty($request->input('datas_para_deletar'))){
                    $datas_calendario = explode(',', $request->input('datas_para_deletar'));
                    $i = 0;
                    while($i < count($datas_calendario)){
                        $calendario_casa = CalendarioCasa::where('id_casa', $request->input('id'))->where('data_aluguel', $datas_calendario[$i])->get();
                        $calendario_casa[0]->delete();
                        $i++;
                    }
                }

                if(!empty($request->input('datas_para_upload'))){
                    $datas_calendario = explode(',', $request->input('datas_para_upload'));
                    $i = 0;
                    while($i < count($datas_calendario)){
                        $calendario_casa = new CalendarioCasa();
                        $calendario_casa->id_casa = $request->input('id');
                        $calendario_casa->data_aluguel = $datas_calendario[$i];
                        $calendario_casa->save();
                        $i++;
                    }
                }
                $year = date("Y");
                $datas_bd = CalendarioCasa::where('id_casa', $request->input('id'))
                            ->whereDate('data_aluguel', '>=', $year.'-01-01')
                            ->select('data_aluguel')->get();
                $msg = 'Casa atualizada com sucesso';
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                    'datas_bd' => $datas_bd,
                );
                return $array;
            }else{
                $msg = "ERRO:<br><br>".$msg;
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                );
                return $array;
            }
        }else{
            $msg = 'ERRO:<br><br>Casa não encontrada';
            $array = array(
                'msg' => $msg,
                'novas_fotos' => $novas_fotos,
            );
            return $array;
        }
    }
}
