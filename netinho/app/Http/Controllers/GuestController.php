<?php

namespace App\Http\Controllers;

use App\Models\Casa;
use App\Models\GaleriaCasa;
use App\Models\GaleriaPasseio4x4;
use App\Models\GaleriaPasseioLancha;
use App\Models\GaleriaPousada;
use App\Models\Passeio4x4;
use App\Models\PasseioLancha;
use App\Models\Pousada;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function sobre_nos(){
        return view('sobre_nos');
    }

    public function index(){
        $array_casa = array();
        $array_pousada = array();
        $array_passeio4x4 = array();
        $array_passeio_lancha = array();
        $array_aux = array();
        $i = 0;
        $qtd = 4;
        $ultima_foto = GaleriaCasa::orderBy('id', 'desc')->first();
        $count_table = GaleriaCasa::count();
        if($count_table < 4){
            $qtd = $count_table;
        }
        while($i < $qtd){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaCasa::where('id', $pic)->select('galeria_casas.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_casa, $galeria[0]->url_imagem);
                $i++;
            }
        }
        $i = 0;
        $qtd = 4;
        $array_aux = array();
        $ultima_foto = GaleriaPousada::orderBy('id', 'desc')->first();
        $count_table = GaleriaPousada::count();
        if($count_table < 4){
            $qtd = $count_table;
        }
        while($i < $qtd){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaPousada::where('id', $pic)->select('galeria_pousadas.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_pousada, $galeria[0]->url_imagem);
                $i++;
            }
        }
        $i = 0;
        $qtd = 8;
        $array_aux = array();
        $ultima_foto = GaleriaPasseio4x4::orderBy('id', 'desc')->first();
        $count_table = GaleriaPasseio4x4::count();
        if($count_table < 8){
            $qtd = $count_table;
        }
        while($i < $count_table){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaPasseio4x4::where('id', $pic)->select('galeria_passeios4x4.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_passeio4x4, $galeria[0]->url_imagem);
                $i++;
            }
        }
        $i = 0;
        $qtd = 8;
        $array_aux = array();
        $ultima_foto = GaleriaPasseioLancha::orderBy('id', 'desc')->first();
        $count_table = GaleriaPasseioLancha::count();
        if($count_table < 8){
            $qtd = $count_table;
        }
        while($i < $count_table){
            $pic = rand(1, $ultima_foto->id);
            $galeria = GaleriaPasseioLancha::where('id', $pic)->select('galeria_passeios_lancha.url_imagem')->get();
            if(empty(in_array($pic, $array_aux)) && count($galeria) > 0){
                array_push($array_aux, $pic);
                array_push($array_passeio_lancha, $galeria[0]->url_imagem);
                $i++;
            }
        }
        return view('index', compact('array_casa', 'array_pousada', 'array_passeio4x4', 'array_passeio_lancha'));
    }

    public function index_pousadas(){
        $search_result = Pousada::orderBy('nome', 'asc')->paginate(10);
        return view('pousadas.home_page_pousadas', compact('search_result'));
    }

    public function index_casas(){
        $search_result = Casa::orderBy('nome', 'asc')->paginate(10);
        return view('casas.home_page_casas', compact('search_result'));
    }

    public function index_passeio4x4(){
        $search_result = Passeio4x4::orderBy('nome', 'asc')->paginate(1);
        return view('passeio4x4.home_page_passeio4x4', compact('search_result'));
    }

    public function index_passeio_lancha(){
        $search_result = PasseioLancha::orderBy('nome', 'asc')->paginate(1);
        return view('passeio_lancha.home_page_passeio_lancha', compact('search_result'));
    }

    public function show(Request $request){
        $select = $request->input('select');
        switch ($select) {
            case "pousada":
                $search_result = Pousada::orderBy('nome', 'asc')->paginate(10);
                return view('pousadas.guest_card_pousada', compact('search_result'))->render();
            break;

            case "casa":
                $search_result = Casa::orderBy('nome', 'asc')->paginate(10);
                return view('casas.guest_card_casa', compact('search_result'))->render();
            break;
        }
    }
}
