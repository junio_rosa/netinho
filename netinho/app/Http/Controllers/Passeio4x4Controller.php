<?php

namespace App\Http\Controllers;

use App\Models\CalendarioPasseio4x4;
use App\Models\GaleriaPasseio4x4;
use App\Models\Passeio4x4;
use Illuminate\Http\Request;

class Passeio4x4Controller extends Controller
{
    public function index(){
        $able = true;
        $passeio4x4 = Passeio4x4::orderBy('nome', 'asc')->paginate(10);
        return view('passeio4x4.home', compact('passeio4x4', 'able'));
    }

    public function store(Request $request)
    {
        $msg = '';
        $existe_passeio = Passeio4x4::where('nome', $request->input('nome'))->get()->count();
        if($existe_passeio > 0){
            $msg .= 'Passeio 4x4 já cadastrado<br>';
        }
        if($msg != ''){
            return $msg;
        }else{
            $ultima_passeio = Passeio4x4::orderBy('id', 'desc')->first();
            $id = 0;
            if($ultima_passeio != NULL){
                $id = $ultima_passeio->id + 1;
            }else{
                $id = 1;
            }
            $passeio = new Passeio4x4();
            $passeio->id = $id;
            $passeio->nome = $request->input('nome');
            $passeio->horarios = $request->input('horarios');
            $passeio->descricao = $request->input('descricao');
            if($request->input('imagem_destaque') != NULL){
                $passeio->imagem_destaque = 'assets/icons/sem_img.png';
            }else{
                $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                $request->file('imagem_destaque')->storeAs('assets/passeios4x4/'.$id.'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                $passeio->imagem_destaque = 'assets/passeios4x4/'.$id.'/imagem_destaque/'.$chave_nome_arquivo;
            }
            $passeio->save();
            $tamanho_galeria = $request->input('tamanho_galeria');
            $i = 0;
            while ($i < $tamanho_galeria) {
                $galeria_p4x4 = new GaleriaPasseio4x4();
                $galeria_p4x4->id_passeio4x4 = $id;
                $controle_loop = true;
                while($controle_loop){
                    $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                    $existe_foto_galeria = GaleriaPasseio4x4::where('id_passeio4x4', $id)->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                    if($existe_foto_galeria == 0){
                        $controle_loop = false;
                        $request->file('galeria_foto_'.$i)->storeAs('assets/passeios4x4/'.$id.'/galeria', $chave_nome_arquivo, 'uploads');
                        $galeria_p4x4->url_imagem = 'assets/passeios4x4/'.$id.'/galeria/'.$chave_nome_arquivo;
                        $galeria_p4x4->save();
                    }
                }
                $i++;
            }
            if(!empty($request->input('datas_para_upload'))){
                $datas_calendario = explode(',', $request->input('datas_para_upload'));
                $i = 0;
                while($i < count($datas_calendario)){
                    $calendario_p4x4 = new CalendarioPasseio4x4();
                    $calendario_p4x4->id_passeio4x4 = $id;
                    $calendario_p4x4->data_aluguel = $datas_calendario[$i];
                    $calendario_p4x4->save();
                    $i++;
                }
            }
            $msg = 'Passeio 4x4 cadastrado com sucesso';
            return $msg;
        }
    }

    public function show(Request $request)
    {
        $input = $request->get('input');
        $select = $request->get('select');
        $autocomplete = 0;
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $select = str_replace('_del', '', $select);
        $passeio4x4 = Passeio4x4::query()->passeio4x4Trash($able)->comparePasseio4x4($input, $select)->passeio4x4Section($autocomplete);
        return view('passeio4x4.table', compact('passeio4x4', 'able'))->render();
    }

    public function autocomplete(Request $request)
    {
        $input = $request->input('input');
        $select = $request->input('select');
        $able = true;
        if(strpos($select, '_del') !== false){
            $able = false;
        }
        $autocomplete = 1;
        $select = str_replace('_del', '', $select);
        $passeio4x4 = Passeio4x4::query()->passeio4x4Trash($able)->comparePasseio4x4($input, $select)->passeio4x4Section($autocomplete);
        return $passeio4x4;
    }

    public function destroy($id)
    {
        $passeio = Passeio4x4::find($id);
        if(isset($passeio)){
            $passeio->delete();
            return 'Passeio 4x4 desabilitado com sucesso';
        }else{
            return 'ERRO:<br><br>Passeio 4x4 não encontrado';
        }
    }

    public function create($id)
    {
        $passeio = Passeio4x4::onlyTrashed()->find($id);
        if(isset($passeio)){
            $passeio->restore();
            return 'Passeio 4x4 habilitado com sucesso';
        }else{
            return 'ERRO:<br><br>Passeio 4x4 não encontrado';
        }
    }

    public function update(Request $request)
    {
        $passeio4x4 = Passeio4x4::find($request->input('id'));
        $msg = '';
        $novas_fotos = array();
        if(isset($passeio4x4)){
            if($msg == ''){
                $passeio4x4->nome = $request->input('nome');
                $passeio4x4->horarios = $request->input('horarios');
                $passeio4x4->descricao = $request->input('descricao');
                if($request->input('imagem_destaque') == NULL){
                    if($passeio4x4->imagem_destaque != 'assets/icons/sem_img.png'){
                        unlink($passeio4x4->imagem_destaque);
                    }
                    $chave_nome_arquivo = md5($request->file('imagem_destaque')->getClientOriginalName(). time()).'.'.$request->input('img_destaque_extensao');
                    $request->file('imagem_destaque')->storeAs('assets/passeios4x4/'.$request->input('id').'/imagem_destaque', $chave_nome_arquivo, 'uploads');
                    $passeio4x4->imagem_destaque = 'assets/passeios4x4/'.$request->input('id').'/imagem_destaque/'.$chave_nome_arquivo;
                }
                $passeio4x4->save();
                $tamanho_galeria = $request->input('tamanho_galeria');
                $i = 0;
                while ($i < $tamanho_galeria) {
                    $galeria_4x4 = new GaleriaPasseio4x4();
                    $galeria_4x4->id_passeio4x4 = $request->input('id');
                    $controle_loop = true;
                    while($controle_loop){
                        $chave_nome_arquivo = md5($request->file('galeria_foto_'.$i)->getClientOriginalName(). time()).'.'.$request->input('galeria_extensao_'.$i);
                        $count_registered = GaleriaPasseio4x4::where('id_passeio4x4', $request->input('id'))->where('url_imagem', 'like', '%'.$chave_nome_arquivo.'%')->get()->count();
                        if($count_registered == 0){
                            $controle_loop = false;
                            $request->file('galeria_foto_'.$i)->storeAs('assets/passeios4x4/'.$request->input('id').'/galeria', $chave_nome_arquivo, 'uploads');
                            $galeria_4x4->url_imagem = 'assets/passeios4x4/'.$request->input('id').'/galeria/'.$chave_nome_arquivo;
                            $galeria_4x4->save();
                            array_push($novas_fotos, $galeria_4x4);
                        }
                    }
                    $i++;
                }
                if($request->input('tamanho_deletar_galeria') > 0){
                    $i = 0;
                    while($i < $request->input('tamanho_deletar_galeria')){
                        $galeria_4x4 = GaleriaPasseio4x4::find($request->input('deletar_galeria_foto_'.$i));
                        unlink($galeria_4x4->url_imagem);
                        $galeria_4x4->delete();
                        $i++;
                    }
                }
                if(!empty($request->input('datas_para_deletar'))){
                    $datas_calendario = explode(',', $request->input('datas_para_deletar'));
                    $i = 0;
                    while($i < count($datas_calendario)){
                        $calendario = CalendarioPasseio4x4::where('id_passeio4x4', $request->input('id'))->where('data_aluguel', $datas_calendario[$i])->get();
                        $calendario[0]->delete();
                        $i++;
                    }
                }

                if(!empty($request->input('datas_para_upload'))){
                    $datas_calendario = explode(',', $request->input('datas_para_upload'));
                    $i = 0;
                    while($i < count($datas_calendario)){
                        $calendario = new CalendarioPasseio4x4();
                        $calendario->id_passeio4x4 = $request->input('id');
                        $calendario->data_aluguel = $datas_calendario[$i];
                        $calendario->save();
                        $i++;
                    }
                }
                $year = date("Y");
                $datas_bd = CalendarioPasseio4x4::where('id_passeio4x4', $request->input('id'))
                            ->whereDate('data_aluguel', '>=', $year.'-01-01')
                            ->select('data_aluguel')->get();
                $msg = 'Passeio 4x4 atualizado com sucesso';
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                    'datas_bd' => $datas_bd,
                );
                return $array;
            }else{
                $msg = "ERRO:<br><br>".$msg;
                $array = array(
                    'msg' => $msg,
                    'novas_fotos' => $novas_fotos,
                );
                return $array;
            }
        }else{
            $msg = 'ERRO:<br><br>Passeio 4x4 não encontrado';
            $array = array(
                'msg' => $msg,
                'novas_fotos' => $novas_fotos,
            );
            return $array;
        }
    }
}
