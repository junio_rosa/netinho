<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGaleriaPasseiosLanchaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria_passeios_lancha', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_passeio_lancha')->unsigned();
            $table->foreign('id_passeio_lancha')->references('id')->on('passeios_lancha');
            $table->string('url_imagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galeria_lanchas');
    }
}
