<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarioCasasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario_casas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_casa')->unsigned();
            $table->foreign('id_casa')->references('id')->on('casas');
            $table->date('data_aluguel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendario_casas');
    }
}
