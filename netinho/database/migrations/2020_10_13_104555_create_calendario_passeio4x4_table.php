<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarioPasseio4x4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario_passeio4x4', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_passeio4x4')->unsigned();
            $table->foreign('id_passeio4x4')->references('id')->on('passeios4x4');
            $table->date('data_aluguel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendario_passeio4x4s');
    }
}
