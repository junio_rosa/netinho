<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGaleriaPasseios4x4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria_passeios4x4', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_passeio4x4')->unsigned();
            $table->foreign('id_passeio4x4')->references('id')->on('passeios4x4');
            $table->string('url_imagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galeria_passeio4x4s');
    }
}
