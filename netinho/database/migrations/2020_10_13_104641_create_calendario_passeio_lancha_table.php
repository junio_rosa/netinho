<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarioPasseioLanchaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario_passeio_lancha', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_passeio_lancha')->unsigned();
            $table->foreign('id_passeio_lancha')->references('id')->on('passeios_lancha');
            $table->date('data_aluguel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendario_passeio_lanchas');
    }
}
