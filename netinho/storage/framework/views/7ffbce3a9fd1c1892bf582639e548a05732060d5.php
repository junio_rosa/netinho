

<?php $__env->startSection('navbar'); ?>
    <?php echo $__env->make('navbars.navbar_guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <section id="casas_cards">
    <?php echo $__env->make('casas.guest_card_casa', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    var img_viewer_control = 0;
    var datas_para_upload = new Array();
    var datas_para_deletar = new Array();
    var datas_bd = new Array();
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page("casa", 'casas_cards');
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Documentos\Sites\netinho\resources\views/casas/home_page_casas.blade.php ENDPATH**/ ?>