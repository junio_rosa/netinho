<div class="form-row">
  <div class="col-5">
    <a class="galeria-preview-thumb" id="details_p4x4_img_link" rel="galeria-preview-thumb" href="" title="Imagem destaque">
      <img class="img-fluid" id="details_p4x4_img">
    </a>
  </div>

  <div class="col-7">
      <p id="details_4x4_horarios"></p>
      <p id="details_4x4_descricao"><p>  
  </div>

  <div class="col-6">
    <label class="font-weight-bold">Calendário:</label>
    <?php echo $__env->make('helpers.calendario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <label class="font-weight-bold text-danger">
      <i class="far fa-square font-weight-bold"></i> 
        <strong>
          Indisponível
        </strong>
    </label>
  </div>

  <div class="col-12 w-100 my-3">
      <label class="font-weight-bold">Descrição:</label>
      <div class="w-100" id="details_cas_descricao"></div>
  </div>

  <?php
      $home_page = true
  ?>
  <?php echo $__env->make('helpers.galeria', compact('home_page'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <div id="snackbar2"></div>
</div><?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio4x4/detalhes.blade.php ENDPATH**/ ?>