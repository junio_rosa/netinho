<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo <?php echo e($casas->count()); ?> casas de <?php echo e($casas->total()); ?> (<?php echo e($casas->firstItem()); ?> a <?php echo e($casas->lastItem()); ?>)
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                <?php
                    $i = $casas->firstItem();
                ?>
                <tr class="text-center">
                    <th>#</th>
                    <th>Casas</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                <?php $__currentLoopData = $casas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="text-center">
                    <td class="align-middle"><?php echo e($i); ?></td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='<?php echo e($cas->imagem_destaque); ?>'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong><?php echo e($cas->nome); ?><br>

                        <strong>
                            Endereço:
                        </strong><?php echo e($cas->endereco); ?><br>

                        <strong>
                            Nº máximo de Ocupantes:
                        </strong><?php echo e($cas->max_ocupantes); ?><br>

                        <strong>
                            Possui sauna:
                        </strong><?php if($cas->sauna): ?>
                            Sim
                            <?php else: ?>
                            Não
                        <?php endif; ?><br>

                        <strong>
                            Possui churrasqueira:
                        </strong><?php if($cas->churrasqueira): ?>
                            Sim
                            <?php else: ?>
                            Não
                        <?php endif; ?><br>

                        <strong>
                            Possui piscina:
                        </strong><?php if($cas->piscina): ?>
                            Sim
                            <?php else: ?>
                            Não
                        <?php endif; ?><br>
                    </td>
                    <td class="align-middle"><?php echo e($cas->id); ?></td>
                    <td class="align-middle">
                        <?php if(isset($able)): ?>
                            <?php if($able): ?>
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_cas(<?php echo e($cas); ?>, <?php echo e($cas->galeria); ?>, <?php echo e($cas->calendario); ?>)">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_cas(<?php echo e($cas); ?>, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            <?php else: ?>
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_cas(<?php echo e($cas); ?>, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <?php
                        $i++;
                    ?>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                <?php echo e($casas->links("pagination::bootstrap-4")); ?>

            </tfoot>
        </table>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/casas/table.blade.php ENDPATH**/ ?>