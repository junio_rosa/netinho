<div class = "modal fade" id = "form_p4x4_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-xl" role = "document">
        <div class = "modal-content">

            <div class="modal-header bg-info">
                <h4 class="text-white" id="form_p4x4_title"></h4>
        
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    
                    <input id="form_p4x4_id" class="d-none">
                    
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Nome:</label>
                        <input class="form-control w-100" type="text" id="form_p4x4_nome" autocomplete="on"
                                onfocusout="validacao_campo('form_p4x4_nome', 'small_px4_nome', 'Campo Validado', 'Nome inválido', 1)">
                        <small id="small_px4_nome" class="text-secondary">
                            Digite o nome ou motorista do passeio
                        </small>
                    </div>
                    
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Horários:</label>
                        <textarea class="form-control editor" type="text" id="form_p4x4_horarios"></textarea>
                    </div>
                    
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Descrição:</label>
                        <textarea class="form-control editor" type="text" id="form_p4x4_descricao"></textarea>
                    </div>
                    
                    <label class="font-weight-bold">Calendário de locação:</label>
                    <?php echo $__env->make('helpers.calendario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    
                    <div class='form-group col-12'>
                        <?php echo $__env->make('helpers.img_destaque', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                    
                    <div class="form-group col-12">
                        <?php echo $__env->make('helpers.galeria', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                </div>

                <div id="snackbar2">Mensagem de erro:</div>
            </div>

            <div class="modal-footer">
                <div class="form-group col-12 text-right">
                    
                    <button class="btn btn-primary mr-1" id="form_p4x4_button" type="button" onclick="action_form_p4x4()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio4x4/modal_form.blade.php ENDPATH**/ ?>