<div class = "navbar navbar-expand-lg bg-light mb-0" id = 'div_navbar'>
    <div class = "container">

        <a class = "navbar-brand" href="/home">
            <img id = "logo_navbar" src = "<?php echo e(asset('assets/icons/logo.png')); ?>">
        </a>
        <button class = "navbar-toggler bg-dark" type = "button" data-toggle = "collapse" data-target = "#navbar_dropdown">
            <i class="fas fa-bars text-white"></i>
        </button>

        <div class = "form-inline collapse navbar-collapse" id = "navbar_dropdown">
            <ul class = "navbar-nav ml-auto">
                <li>
                    <a class = "nav-link mr-2 font-weight-bold"
                    href = "/pousadas">
                        Pousadas
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 font-weight-bold"
                    href = "/casas">
                        Casas
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 font-weight-bold"
                    href = "/passeio4x4">
                        Passeio 4x4
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 font-weight-bold"
                    href = "/passeio_lancha">
                        Passeio Lancha
                    </a>
                </li>
                <li>
                    <a class = "nav-link mr-2 font-weight-bold"
                    href = "/sobre_nos">
                        Sobre Nós
                    </a>
                </li>
                <li>
                    <a class="nav-link mr-2 font-weight-bold" href="https://www.facebook.com/resolveescarpas" target="_blank">
                        <i class="fab fa-facebook-square"></i> Facebook
                    </a>
                </li>
                <li>
                    <a class="nav-link mr-2 font-weight-bold" href="https://www.instagram.com/resolveescarpas/" target="_blank">
                        <i class="fab fa-instagram"></i> Instagram
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a class = "nav-link dropdown-toggle mr-2 font-weight-bold"
                        href = "#" id = "admin"
                        role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <?php echo e(Auth::user()->name); ?>

                    </a>
                    <div class="dropdown-menu" aria-labelledby="admin">
                        <a class="dropdown-item cursor-pointer"
                            onclick="modal_atualizar_perfil(<?php echo e(Auth::user()); ?>)">
                            Atualizar Perfil
                        </a>
                        <a class="dropdown-item cursor-pointer"
                            href="<?php echo e(route('logout')); ?>"
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            Logout
                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                <?php echo csrf_field(); ?>
                            </form>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<hr class="my-0">
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/navbars/navbar_auth.blade.php ENDPATH**/ ?>