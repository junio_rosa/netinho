<?php $__currentLoopData = $search_result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p4x4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <div class="col-12 col-sm-6 col-md-4 col-lg-3 d-flex align-items-stretch">
    <div class="card my-3">
      <div class="card-title">
        <img class="card-img cursor-pointer" src="<?php echo e($p4x4->imagem_destaque); ?>">
      </div>
      <div class="card-body text-center">
          <div class="col-12 font-weight-bold">
            Passeio 4x4
          </div>
      </div>
      <div class="card-footer">
        <div class="col-12">
            <button class="btn btn-sm btn-info text-white" onclick="open_details_passeio4x4(<?php echo e($p4x4); ?>, <?php echo e($p4x4->galeria); ?>, <?php echo e($p4x4->calendario); ?>)">
                <i class="fas fa-info-circle"></i> Detalhes
            </button>
          </div>
      </div>
    </div>
  </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<div class="col-12 w-100">
  <?php echo e($search_result->links("pagination::bootstrap-4")); ?>

</div>

<section>
    <?php echo $__env->make('passeio4x4.modal_detalhes', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</section><?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio4x4/guest_card_passeio4x4.blade.php ENDPATH**/ ?>