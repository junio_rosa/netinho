

<?php $__env->startSection('navbar'); ?>
    <?php if(Auth::check()): ?>
        <?php echo $__env->make('navbars.navbar_auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>    
    <?php else: ?>
        <?php echo $__env->make('navbars.navbar_guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="form-row">
        <h1 class="col-12 mt-5">
            Resolve Escarpas
            <hr>
        </h1>
        <h3 class="col-12">
            Conheça a nossa empresa
        </h3>
        <h5 class="col-12 text-justify">
            Somos uma empresa nascida em Capitólio, fundada por pessoas que sempre tiveram como motivação a vontade de compartilhar com o Brasil e o mundo a riqueza do lugar no qual nasceram e cresceram.
            Entendemos o turismo não apenas como uma atividade econômica, mas como um compartilhamento de experiências e uma forma de valorização não só da beleza, mas também da cultura e história locais. 
            Como cidadãos capitolinos, almejamos que os nossos clientes vivam intensamente a experiência turística, apresentando com muita dedicação o lugar pelo qual temos tanto amor. 
            Além disso, priorizamos a qualidade e a segurança dos nossos serviços, para que aqueles que nos escolheram se sintam livres para aproveitar seus dias de folga sem maiores preocupações. 
            <br> <br>
            Ofertamos aos nossos clientes passeios com segurança, conforto e prazer, assim, esperamos que Capitólio se torne um destino ao qual desejem sempre retornar!
            <br><br><br>
        </h5>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(function(){
        
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Documentos\Sites\netinho\resources\views/sobre_nos.blade.php ENDPATH**/ ?>