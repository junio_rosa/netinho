<?php $__env->startSection('navbar'); ?>
    <?php echo $__env->make('navbars.navbar_guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center my-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?php echo e(__('Esqueceu a senha:')); ?></div>

                <div class="card-body">
                    <form method="POST" id='form_esqueceu_senha' action="<?php echo e(route('password.email')); ?>">
                        <div class="row">
                            <?php echo csrf_field(); ?>
                            <div class="form-group col-12">
                                <label>E-mail:</label>
                                <input class="form-control" type="text" id="email" name="email"
                                    onfocusout="validacao_email('email', 'small_email', 'Campo Validado', 'Campo e-mail inválido')">
                                <small id="small_email" class="text-secondary">
                                    Digite um e-mail válido
                                </small>
                            </div>

                            <div class="col-12 my-3 text-center" id="div_alert">
                                <div class="alert alert-primary" role="alert" id="type_alert">
                                </div>
                            </div>

                            <div class="form-group col-12 text-right">
                                <a href="/login" class="btn btn-success">
                                    <i class="fas fa-arrow-circle-left"></i> Voltar
                                </a>

                                <button class="btn btn-primary mr-1" type="submit">
                                    <i class="fas fa-external-link-square-alt"></i> <?php echo e(__('Enviar um link para redefinir a senha')); ?>

                                </button>
                            </div>
                        </div>
                    </form>

                    <div id="snackbar2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(function(){
        document.getElementById('div_alert').classList.add('d-none');
        <?php if(isset($msg)): ?>
            mensagem = '';
            <?php $__currentLoopData = $msg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                mensagem += "<?php echo e($m); ?>"+'<br>';
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            document.getElementById('div_alert').classList.remove('d-none');
            document.getElementById('type_alert').classList.add("<?php echo e($type); ?>");
            document.getElementById('type_alert').innerHTML = mensagem;
        <?php endif; ?>
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Documentos\Sites\netinho\resources\views/auth/passwords/email.blade.php ENDPATH**/ ?>