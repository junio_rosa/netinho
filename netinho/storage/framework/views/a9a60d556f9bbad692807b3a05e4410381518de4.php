
<div id="slideshow" class="carousel slide w-100" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_1.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_2.jpg" alt="">
    </div>
    <div class="carousel-item active">
      <img class="d-block w-100" src="assets/slideshow/slideshow_3.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_4.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_5.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_6.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_7.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_8.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_9.jpg" alt="">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/slideshow/slideshow_10.jpg" alt="">
    </div>
  </div>
  <a class="carousel-control-prev" href="#slideshow" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#slideshow" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<div class="container my-5">
  <h1 class="text-center" style="font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;">
    Galeria
  </h1>
  <div class="form-row">
    <?php $__currentLoopData = $array_casa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
      <div class='col-12 col-sm-6 col-md-4 col-lg-3 my-3'>
          <a class='home-galeria-thumb' rel='home-galeria-thumb' href=<?php echo e($c); ?> title="Galeria Preview">
            <img class='img-home-galeria cursor-pointer' src=<?php echo e($c); ?> onclick='galeria_home_page()'>
          </a>
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php $__currentLoopData = $array_pousada; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class='col-12 col-sm-6 col-md-4 col-lg-3 my-3'>
          <a class='home-galeria-thumb' rel='home-galeria-thumb' href=<?php echo e($p); ?> title="Galeria Preview">
            <img class='img-home-galeria cursor-pointer' src=<?php echo e($p); ?> onclick='galeria_home_page()'>
          </a>
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php $__currentLoopData = $array_passeio4x4; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p4x4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class='col-12 col-sm-6 col-md-4 col-lg-3 my-3'>
          <a class='home-galeria-thumb' rel='home-galeria-thumb' href=<?php echo e($p4x4); ?> title="Galeria Preview">
            <img class='img-home-galeria cursor-pointer' src=<?php echo e($p4x4); ?> onclick='galeria_home_page()'>
          </a>
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php $__currentLoopData = $array_passeio_lancha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p_lancha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class='col-12 col-sm-6 col-md-4 col-lg-3 my-3'>
          <a class='home-galeria-thumb' rel='home-galeria-thumb' href=<?php echo e($p_lancha); ?> title="Galeria Preview">
            <img class='img-home-galeria cursor-pointer' src=<?php echo e($p_lancha); ?> onclick='galeria_home_page()'>
          </a>
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>


<div class="video-area my-5">
  <img class="w-100" src="assets/icons/video.jpg">
  <div class="centered">
    <img class="cursor-pointer img-thumb" src="assets/icons/play_button.gif" onclick="open_video()">
    <figcaption class="text-white">
      <h3 class="d-none d-lg-block" style="font-family: 'Comic Sans MS', cursive, sans-serif;">
        Apresentação
      </h3>
    </figcaption>
  </div>
</div>


<div class="container my-5">
  <div class="form-row text-center">
    <div class="col-12 col-md-4">
      <img class="img-thumb" src="assets/icons/passeios.png" alt="">
      <figcaption>
        <h3 class="my-3">
          Passeios
        </h3>
        <h5 class="text-justify mb-5 mx-3 font-italic">
          Viajar: primeiro te deixa sem palavras, depois te transforma em um contador de histórias.
        </h5>
      </figcaption>
    </div>
    <div class="col-12 col-md-4">
      <img class="img-thumb" src="assets/icons/hospedagens.png" alt="">
      <figcaption>
        <h3 class="my-3">
          Hospedagens
        </h3>
        <h5 class="text-justify mb-5 mx-3 font-italic">
          O nosso coração é como um hotel. Entram e saem pessoas a toda a hora, mas àquelas pessoas que vivem lá, e aquelas que partiram, mas mesmo assim tem aquele espacinho especial para elas.
        </h5>
      </figcaption>
    </div>
    <div class="col-12 col-md-4">
      <img class="img-thumb" src="assets/icons/guia.png" alt="">
      <figcaption>
        <h3 class="my-3">
          Guias de turismo
        </h3>
        <h5 class="text-justify mb-5 mx-3 font-italic">
          Fazer TURISMO é como entrar num PORTAL DO TEMPO e eleger o local desejado, no momento certo
        </h5>
      </figcaption>
    </div>
  </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/layouts/home_page.blade.php ENDPATH**/ ?>