<div class = "modal fade" id = "modal_update_user" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-lg" role = "document">
        <div class = "modal-content">
            <div class = "modal-header col-12 my-4">
                <ul class="nav">
                    <li class = "nav-item">
                        
                        <h4 class="text-muted">Atualizar Dados de Perfil</h4>
                    </li>
                </ul>

                <ul class="nav justify-content-end ml-auto">
                    <li class = "nav-item">
                        <button class = "btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-times fa-2x"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class = "modal-body">
                <div class="row">
                    
                    <input type="text" id="updateUser_id">
                    
                    <div class="form-group col-12">
                        <label>Nome:</label>
                        <input class="form-control" type="text" id="updateUser_nome"
                            onfocusout="validacao_campo('updateUser_nome', 'updateUser_small_nome', 'Campo Validado', 'Campo nome inválido', 1)">
                        <small id="updateUser_small_nome" class="text-secondary">
                            Digite o nome de usuário
                        </small>
                    </div>
                    
                    <div class="form-group col-12">
                        <label>E-mail:</label>
                        <input class="form-control" type="text" id="updateUser_email"
                            onfocusout="validacao_email('updateUser_email', 'updateUser_small_email', 'Campo Validado', 'Campo e-mail inválido')">
                        <small id="updateUser_small_email" class="text-secondary">
                            Digite um e-mail válido
                        </small>
                    </div>
                    
                    <div class="form-group col-12">
                        <label>Senha:</label>
                        <div class="input-group">
                            <input class="form-control" type="password" id="updateUser_senha"
                                onfocusout="validacao_campo('updateUser_senha', 'updateUser_small_senha', 'Campo Validado', 'Campo senha inválido', 8)">
                            <div class="input-group-append">
                                <button class="btn btn-secondary rounded-right" onclick="mostrar_password('updateUser_senha', 'updateUser_button_eye_senha')" id="updateUser_button_eye_senha"  type="button">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
                        </div>
                        <small id="updateUser_small_senha" class="text-secondary">
                            Digite uma senha de no mínimo 8 caracteres
                        </small>
                    </div>
                    
                    <div class="form-group col-12">
                        <label>Repita a senha:</label>
                        <div class="input-group">
                            <input class="form-control" type="password" id="updateUser_repita_senha"
                            onfocusout="validacao_senhas('updateUser_senha', 'updateUser_repita_senha', 'updateUser_small_senha', 'updateUser_small_repita_senha')">
                            <div class="input-group-append">
                                <button class="btn btn-secondary rounded-right" onclick="mostrar_password('updateUser_repita_senha', 'updateUser_button_eye_repita_senha')" id="updateUser_button_eye_repita_senha"  type="button">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
                        </div>
                        <small id="updateUser_small_repita_senha" class="text-secondary">
                            Digite novamente a senha
                        </small>
                    </div>

                    <div class="form-group col-12 text-right">
                        <button class="btn btn-primary" type="button" onclick="action_update_user()">
                            <i class="fas fa-sync"></i> Atualizar
                        </button>

                        <button class="btn btn-danger mr-1" type="button" data-dismiss="modal">
                            <i class="fas fa-times-circle"></i> Fechar
                        </button>
                    </div>
                </div>

                <div id="snackbar1">Mensagem de erro:</div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/auth/update_modal.blade.php ENDPATH**/ ?>