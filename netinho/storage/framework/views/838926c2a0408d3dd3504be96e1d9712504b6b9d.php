<div class="container">
  <div class="form-row">
    <?php $__currentLoopData = $search_result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pou): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-12 col-sm-6 col-md-4 col-lg-3 d-flex align-items-stretch">
        <div class="card my-3">
          <div class="card-title">
            <img class="card-img cursor-pointer" src="<?php echo e(asset($pou->imagem_destaque)); ?>">
          </div>
          <div class="card-body text-center">
              <div class="col-12">
                <h3 class="font-weight-bold">
                  <?php echo e($pou->nome); ?>

                </h3>
              </div>
          </div>
          <div class="card-footer">
            <div class="col-12">
                <button class="btn btn-sm btn-info text-white" onclick="open_modal_pousada_details(<?php echo e($pou); ?>, <?php echo e($pou->galeria); ?>)">
                    <i class="fas fa-info-circle"></i> Detalhes
                </button>
            </div>
          </div>
        </div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <div class="col-12 w-100">
      <?php echo e($search_result->links("pagination::bootstrap-4")); ?>

    </div>

    <section>
        <?php echo $__env->make('pousadas.modal_detalhes', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </section>
  </div>
</div><?php /**PATH D:\Documentos\Sites\netinho\resources\views/pousadas/guest_card_pousada.blade.php ENDPATH**/ ?>