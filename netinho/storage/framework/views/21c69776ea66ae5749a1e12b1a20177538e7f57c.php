<?php $__env->startSection('navbar'); ?>
    <?php echo $__env->make('navbars.navbar_auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('layouts.home_page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    var img_viewer_control;
    var datas_bd = new Array();
    var datas_para_deletar = new Array();
    var datas_para_upload = new Array();
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page();
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Documentos\Sites\netinho\resources\views/home.blade.php ENDPATH**/ ?>