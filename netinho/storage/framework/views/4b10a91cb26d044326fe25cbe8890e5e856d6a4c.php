<div class = "modal fade" id = "form_cas_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-xl" role = "document">
        <div class = "modal-content">

            <div class="modal-header bg-info">
                <h4 class="text-white" id="form_cas_title"></h4>
        
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <div class = "modal-body">
                <div class="form-row">
                    
                    <input id="form_cas_id" class="d-none">
                    
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Nome:</label>
                        <input class="form-control w-100" type="text" id="form_cas_nome" autocomplete="on"
                                onfocusout="validacao_campo('form_cas_nome', 'small_cas_nome', 'Campo Validado', 'Nome inválido', 1)">
                        <small id="small_cas_nome" class="text-secondary">
                            Digite o nome da casa
                        </small>
                    </div>
                    
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Endereço:</label>
                        <input class="form-control w-100" type="text" id="form_cas_endereco" autocomplete="on"
                                onfocusout="validacao_campo('form_cas_endereco', 'small_cas_endereco', 'Campo Validado', 'Endereço inválido', 1)">
                        <small id="small_cas_endereco" class="text-secondary">
                            Digite o endereço da casa
                        </small>
                    </div>
                    
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Nº máximo de ocupantes:</label>
                        <input class="form-control w-100"
                                type="input"
                                id="form_cas_ocupantes" autocomplete="on"
                                onfocusout="validacao_campo('form_cas_ocupantes', 'small_cas_ocupantes', 'Campo Validado', 'Campo ocupantes inválido', 1)">
                        <small id="small_cas_ocupantes" class="text-secondary">
                            Digite o número máximo de ocupantes
                        </small>
                    </div>
                    
                    <div class="form-group col-12 col-lg-4 ml-4 text-left ml-lg-0 text-lg-center">
                        <input type="checkbox" class="form-check-input" id="form_cas_churrasqueira">
                        <label class="font-weight-bold" for="form_cas_churrasqueira">Possui churrasqueira</label>
                    </div>
                    
                    <div class="form-group col-12 col-lg-4 ml-4 text-left ml-lg-0 text-lg-center">
                        <input type="checkbox" class="form-check-input" id="form_cas_piscina">
                        <label class="font-weight-bold" for="form_cas_piscina">Possui piscina</label>
                    </div>
                    
                    <div class="form-group col-12 col-lg-4 ml-4 text-left ml-lg-0 text-lg-center">
                        <input type="checkbox" class="form-check-input" id="form_cas_sauna">
                        <label class="font-weight-bold" for="form_cas_sauna">Possui sauna</label>
                    </div>
                    
                    <div class="form-group col-12">
                        <label class="font-weight-bold">Descrição:</label>
                        <textarea class="form-control editor" type="text" id="form_cas_descricao"></textarea>
                    </div>
                    
                    <label class="font-weight-bold">Calendário:</label>
                    <?php echo $__env->make('helpers.calendario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    
                    <div class='form-group col-12'>
                        <?php echo $__env->make('helpers.img_destaque', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                    
                    <div class="form-group col-12">
                        <?php echo $__env->make('helpers.galeria', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                </div>

                <div id="snackbar2">Mensagem de erro:</div>
            </div>

            <div class="modal-footer">
                <div class="form-group col-12 text-right">
                    
                    <button class="btn btn-primary mr-1" id="form_cas_button" type="button" onclick="action_form_cas()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/casas/modal_form.blade.php ENDPATH**/ ?>