<div class="container">
    <div class="form-row">
        <div class="col-12">
            <h1>
                <?php echo e($search_result[0]->nome); ?>

            </h1>
        </div>

        <div class="col-12 my-3">
            <?php
                echo($search_result[0]->horarios);
            ?>
        </div>

        <div class="col-12 my-3">
            <?php
                echo($search_result[0]->descricao);
            ?>
        </div>

        <div class="col-12">
            <?php
                $home_page = true;
            ?>
            <?php echo $__env->make('helpers.galeria', compact('home_page'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="col-12 col-md-8 my-5">
            <label class="font-weight-bold">Calendário:</label>
            <?php echo $__env->make('helpers.calendario', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div id="snackbar2"></div>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio4x4/guest_passeio4x4.blade.php ENDPATH**/ ?>