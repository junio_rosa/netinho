<div class = "modal fade" id = "confirm_cas_modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog modal-md" role = "document">
        <div class = "modal-content">

            <div class="modal-header bg-info">
                <h4 class="text-white" id="confirm_p4x4_title"></h4>
        
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <div class = "modal-body">
                <div class="row">
                    <div class="col-12 mb-5" id="confirm_p4x4_msg">
                    </div>
                    <input id="confirm_p4x4_id" class="d-none">
                </div>

                <div id="snackbar3">Mensagem de erro:</div>
            </div>

            <div class="modal-footer">
                <div class="form-group col-12 text-right">
                    <button class="btn btn-primary mr-1" id='confirm_p4x4_button' type="button"
                        onclick="action_confirm_p4x4()">
                    </button>

                    <button class="btn btn-danger" data-dismiss = "modal">
                        <i class="fas fa-times-circle text-white"></i> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio4x4/modal_confirmacao.blade.php ENDPATH**/ ?>