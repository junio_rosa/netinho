

<?php $__env->startSection('navbar'); ?>
    <?php echo $__env->make('navbars.navbar_guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <section id="pousadas_cards">
    <?php echo $__env->make('pousadas.guest_card_pousada', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    var img_viewer_control = 0;
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page("pousada", 'pousadas_cards');
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Documentos\Sites\netinho\resources\views/pousadas/home_page_pousadas.blade.php ENDPATH**/ ?>