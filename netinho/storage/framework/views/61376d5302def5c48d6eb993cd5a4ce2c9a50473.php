<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel = "icon" href = "<?php echo e(asset('assets/icons/favicon.ico')); ?>">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/fancybox.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/fancybox-thumbs.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/footer.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/img_fonts.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/popup.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/snackbars.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/styles.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/textarea.css')); ?>" rel="stylesheet">
    <title>Resolve Escarpas</title>
</head>

<header>
    <div id="header_navbar">
        <?php if (! empty(trim($__env->yieldContent('navbar')))): ?>
            <?php echo $__env->yieldContent('navbar'); ?>
        <?php endif; ?>
    </div>
</header>

<body>
    <main role="main">
        <?php if (! empty(trim($__env->yieldContent('content')))): ?>
            <?php echo $__env->yieldContent('content'); ?>
        <?php endif; ?>
    </main>

    <section>
        <?php echo $__env->make('auth.update_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </section>
</body>

<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer_widget w-100">
                        <div class="footer_logo">
                            <img id="logo_navbar" src="<?php echo e(asset('assets/icons/logo_footer.png')); ?>" alt="">
                        </div>
                        <h3 class="text-white">RESOLVE ESCARPAS</h3>
                        <p>R. Dr. Avelino de Queiroz, 1235 - Centro <br> Capitólio - MG, CEP: 37930-000 <br>
                            <a href="tel:+37999437100" target="_blank"><i class="fas fa-phone"></i> Agência: (37) 99943-7100</a> <br>
                            <a href="tel:+35999974148" target="_blank"><i class="fas fa-phone"></i> Neto: (35) 9999-4148</a> <br>
                            <a href="tel:+37999837171" target="_blank"><i class="fas fa-phone"></i> Gustavo Silva: (37) 99983-7171</a> <br>
                            <a href="mailto:netosovemar@gmail.com" target="_blank"><i class="far fa-envelope"></i> netosovemar@gmail.com</a>
                        </p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/resolveescarpas" target="_blank">
                                        <i class="fab fa-facebook-square"></i> Facebook <br>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/resolveescarpas/" target="_blank">
                                        <i class="fab fa-instagram"></i> Instagram
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer_widget w-100">
                        <h3 class="footer_title">
                            Sobre Nós:
                        </h3>
                        <h5 class="text-justify text-white">
                            Somos uma empresa nascida em Capitólio, 
                            fundada por pessoas que sempre tiveram como motivação 
                            a vontade de compartilhar com o Brasil...
                        </h5> <br>
                        <a href="/sobre_nos" class="text-white">
                            <h5>
                                <i class="fas fa-chevron-circle-right"></i> Continuar Lendo
                            </h5>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer_widget w-100">
                        <h3 class="footer_title">
                            Como Chegar:
                        </h3>

                        <div class="w-100">
                            <iframe style="max-width: 500px; max-height: 300px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3734.491357687657!2d-46.058514485617785!3d-20.608819465096257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b43d9d5d74c95b%3A0x830e5ef9ffd23d70!2sResolve%20Escarpas!5e0!3m2!1spt-BR!2sbr!4v1600439087223!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        © <?php echo e(date('Y')); ?> Copyright: <a href="https://www.linkedin.com/in/j%C3%BAnio-rosa-94b5731b2/"> Júnio César Rosa</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="<?php echo e(asset('js/app.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/auth.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/calendario.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/casas.js')); ?>" type="text/javascript"></script>>
<script src="<?php echo e(asset('js/fancybox.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/fancybox-pack.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/fancybox-thumbs.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/galeria.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/helpers.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/home_page.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/mask.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/passeio_4x4.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/passeio_lancha.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/popup.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/pousadas.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/textarea.js')); ?>" type="text/javascript"></script>
<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "<?php echo e(csrf_token()); ?>"
        }
    });

    /* DESABILITAR INSPECIONADOR DE CÓDIGO,
    ATIVAR QUANDO O SITE TIVER NO SERVIDOR */
    /* $(document).keydown(function(e){
        if(e.which === 123){
        return false;
        }
    }); */

</script>

<?php if (! empty(trim($__env->yieldContent('javascript')))): ?>
    <?php echo $__env->yieldContent('javascript'); ?>
<?php endif; ?>
</html>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/layouts/app.blade.php ENDPATH**/ ?>