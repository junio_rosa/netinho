<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo <?php echo e($pousadas->count()); ?> pousadas de <?php echo e($pousadas->total()); ?> (<?php echo e($pousadas->firstItem()); ?> a <?php echo e($pousadas->lastItem()); ?>)
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                <?php
                    $i = $pousadas->firstItem();
                ?>
                <tr class="text-center">
                    <th>#</th>
                    <th>Pousada</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                <?php $__currentLoopData = $pousadas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pou): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="text-center">
                    <td class="align-middle"><?php echo e($i); ?></td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='<?php echo e($pou->imagem_destaque); ?>'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong><?php echo e($pou->nome); ?><br>

                        <strong>
                            Endereço:
                        </strong><?php echo e($pou->endereco); ?><br>

                        <strong>
                            Tamanho:
                        </strong><?php echo e($pou->tamanho); ?><br>

                        <strong>
                            Diária:
                        </strong><?php echo e($pou->diaria); ?><br>

                        <strong>
                            Telefone:
                        </strong><?php echo e($pou->telefone); ?><br>

                        <strong>
                            Possui piscina:
                        </strong><?php if($pou->piscina): ?>
                            Sim
                            <?php else: ?>
                            Não
                        <?php endif; ?><br>
                    </td>
                    <td class="align-middle"><?php echo e($pou->id); ?></td>
                    <td class="align-middle">
                        <?php if(isset($able)): ?>
                            <?php if($able): ?>
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_pou(<?php echo e($pou); ?>, <?php echo e($pou->galeria); ?>)">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_pou(<?php echo e($pou); ?>, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            <?php else: ?>
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_pou(<?php echo e($pou); ?>, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <?php
                        $i++;
                    ?>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                <?php echo e($pousadas->links("pagination::bootstrap-4")); ?>

            </tfoot>
        </table>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/pousadas/table.blade.php ENDPATH**/ ?>