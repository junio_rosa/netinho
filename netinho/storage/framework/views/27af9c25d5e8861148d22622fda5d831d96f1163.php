<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo <?php echo e($passeio_lancha->count()); ?> passeios lancha de <?php echo e($passeio_lancha->total()); ?> (<?php echo e($passeio_lancha->firstItem()); ?> a <?php echo e($passeio_lancha->lastItem()); ?>)
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                <?php
                    $i = $passeio_lancha->firstItem();
                ?>
                <tr class="text-center">
                    <th>#</th>
                    <th>Passeio lancha</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                <?php $__currentLoopData = $passeio_lancha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lancha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="text-center">
                    <td class="align-middle"><?php echo e($i); ?></td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='<?php echo e($lancha->imagem_destaque); ?>'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong><?php echo e($lancha->nome); ?><br>
                    </td>
                    <td class="align-middle"><?php echo e($lancha->id); ?></td>
                    <td class="align-middle">
                        <?php if(isset($able)): ?>
                            <?php if($able): ?>
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_lancha(<?php echo e($lancha); ?>, <?php echo e($lancha->galeria); ?>, <?php echo e($lancha->calendario); ?>)">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_lancha(<?php echo e($lancha); ?>, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            <?php else: ?>
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_lancha(<?php echo e($lancha); ?>, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <?php
                        $i++;
                    ?>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                <?php echo e($passeio_lancha->links("pagination::bootstrap-4")); ?>

            </tfoot>
        </table>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio_lancha/table.blade.php ENDPATH**/ ?>