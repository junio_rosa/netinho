

<?php $__env->startSection('navbar'); ?>
    <?php echo $__env->make('navbars.navbar_guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <section id="passeio_lancha_cards">
        <?php echo $__env->make('passeio_lancha.guest_card_passeio_lancha', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    var img_viewer_control = 0;
    var datas_para_upload = new Array();
    var datas_para_deletar = new Array();
    var datas_bd = new Array();
    var paginacao_home = 1;

    $(function(){
        paginacao_home = 1;
        var calendario = <?php echo json_encode($search_result[0]->calendario); ?>;
        var i = 0;
        while(i < calendario.length){
            datas_bd.push(calendario[i].data_aluguel);
            i++;
        }
        montar_calendario(new Date(), 1);
        var galeria = <?php echo json_encode($search_result[0]->galeria); ?>;
        var img_destaque = <?php echo json_encode($search_result[0]->imagem_destaque); ?>;
        i = 0;
        document.getElementById('galeria_divContent').innerHTML = "";
        galeriaImgPreview('../'+img_destaque, img_destaque, 0, 1);
        while(i < galeria.length){
            galeriaImgPreview('../'+galeria[i].url_imagem, galeria[i], 0, 1);
            i++;
        }
        visualizar_mais();
    });

    $(document).on('click', '.pagination a', function(event){
        event.preventDefault();
        var pagina_atual = $(this).attr('href').split('page=')[1];
        paginacao_home = pagina_atual;
        fetch_data_home_page("passeio4x4", 'passeio4x4_cards');
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio_lancha/home_page_passeio_lancha.blade.php ENDPATH**/ ?>