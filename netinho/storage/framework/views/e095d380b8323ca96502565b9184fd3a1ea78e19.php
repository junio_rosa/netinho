<?php $__env->startComponent('mail::message'); ?>
<h3>Olá, <?php echo e($user->name); ?></h3>
<div align="justify">
    Recentemente você fez uma requisição para redefinir senha na sua conta do site Resolve Escarpas.
    Sendo assim, clique no botão abaixo caso deseje redefini-la.
</div>

<?php $__env->startComponent('mail::button', ['url' => url("password/reset/$token?email=$user->email")]); ?>
Redefinir Senha
<?php if (isset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e)): ?>
<?php $component = $__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e; ?>
<?php unset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

<div align="justify">
    Se você não fez essa requisição para redefinição de senha,
    por favor ignore esse e-mail ou responda, para o nosso suporte
    corrigir este erro. <br>
</div>
<br><br>

Atenciosamente,<br>
Suporte Resolve Escarpas.
<?php if (isset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d)): ?>
<?php $component = $__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d; ?>
<?php unset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/emails/reset_mail.blade.php ENDPATH**/ ?>