
<div class="table-responsive my-3 d-none">
    <table class="table table-ordered table-hover" id="table_galeria">
        
        <tbody>
            <thead>
                <tr class="text-center">
                    <th>Código</th>
                    <th>Nome Arquivo</th>
                    <th>Extensão</th>
                    <th>Uploaded</th>
                    <th>ID BD</th>
                    <th>Pos Array File</th>
                    <th>Deletado</th>
                </tr>
            </thead>
        </tbody>
    </table>
</div>



<div class="col-12">
    <label class="font-weight-bold">Galeria:</label>
    <?php if(!isset($home_page)): ?>
    <div class="custom-file">
        <input type="file" class="custom-file-input"  name="inputGaleria[]" id="inputGaleria" multiple>
        <label class="custom-file-label" for="inputGaleria" data-browse="Procurar">
            Clique aqui para fazer o upload
        </label>
    </div>
    <?php endif; ?>
</div>


<div class="div-galeria col-12 
    <?php if(!isset($home_page)): ?>
        mt-4"
    <?php else: ?>
        "
    <?php endif; ?>
    >
    <div class="card w-100 border-0">
        <div class="card-title text-right border-0">
            <button class="btn btn-success my-2" onclick="visualizar_mais()">
                <i class="fas fa-plus-circle"></i> Visualizar mais
            </button>
            <button class="btn btn-primary my-2" onclick="visualizar_menos()">
                <i class="fas fa-minus-circle"></i> Visualizar menos
            </button>
        </div>
        <hr>
        <div class="card-body max-height-galeria">
            <div class="form-row" id="galeria_divContent">
                
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/helpers/galeria.blade.php ENDPATH**/ ?>