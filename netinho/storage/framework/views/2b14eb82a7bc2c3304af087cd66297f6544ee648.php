<div class="card-body">
    <div class="table-responsive">
        <h4 class="card-title text-secondary">
            Exibindo <?php echo e($passeio4x4->count()); ?> passeios 4x4 de <?php echo e($passeio4x4->total()); ?> (<?php echo e($passeio4x4->firstItem()); ?> a <?php echo e($passeio4x4->lastItem()); ?>)
        </h4>
        <table class="table table-ordered table-hover">
            <thead>
                <?php
                    $i = $passeio4x4->firstItem();
                ?>
                <tr class="text-center">
                    <th>#</th>
                    <th>Passeio 4x4</th>
                    <th>Informações</th>
                    <th>Código</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                <?php $__currentLoopData = $passeio4x4; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p4x4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="text-center">
                    <td class="align-middle"><?php echo e($i); ?></td>
                    <td class="align-middle"><img class= 'rounded img-thumb' src='<?php echo e($p4x4->imagem_destaque); ?>'></td>
                    <td class="align-middle text-left">
                        <strong>
                            Nome:
                        </strong><?php echo e($p4x4->nome); ?><br>
                    </td>
                    <td class="align-middle"><?php echo e($p4x4->id); ?></td>
                    <td class="align-middle">
                        <?php if(isset($able)): ?>
                            <?php if($able): ?>
                                <button class="btn btn-sm btn-success"
                                    onclick="open_form_p4x4(<?php echo e($p4x4); ?>, <?php echo e($p4x4->galeria); ?>, <?php echo e($p4x4->calendario); ?>)">
                                    <i class="far fa-edit"></i> Editar
                                </button><br>
                                <button class="btn btn-sm btn-dark mt-1"
                                    onclick="open_confirm_p4x4(<?php echo e($p4x4); ?>, 1)">
                                    <i class="far fa-trash-alt"></i> Desabilitar
                                </button>
                            <?php else: ?>
                                <button class="btn btn-sm btn-info text-white"
                                    onclick="open_confirm_p4x4(<?php echo e($p4x4); ?>, 0)">
                                    <i class="fas fa-trash-restore"></i> Habilitar
                                </button>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <?php
                        $i++;
                    ?>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>

<div class="table-responsive mb-5">
    <div class="card-footer">
        <table class="table table-ordered table-hover">
            <tfoot>
                <?php echo e($passeio4x4->links("pagination::bootstrap-4")); ?>

            </tfoot>
        </table>
    </div>
</div>
<?php /**PATH D:\Documentos\Sites\netinho\resources\views/passeio4x4/table.blade.php ENDPATH**/ ?>